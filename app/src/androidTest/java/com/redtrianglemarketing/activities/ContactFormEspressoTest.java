package com.redtrianglemarketing.activities;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.redtrianglemarketing.R;
import com.redtrianglemarketing.activity.ContactFormActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ContactFormEspressoTest {


    @Rule
    public ActivityTestRule<ContactFormActivity> mActivityRule = new ActivityTestRule<>(ContactFormActivity.class);

    @Test
    public void emptyEditCheck() {

        onView(withId(R.id.edtFirstName)).perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.btnSubmit)).perform(click());
        onView(withId(R.id.edtLastName)).perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.btnSubmit)).perform(click());
        onView(withId(R.id.edtEmail)).perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.btnSubmit)).perform(click());
        onView(withId(R.id.edtMessage)).perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.btnSubmit)).perform(click());


    }


}