package com.redtrianglemarketing.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.ToolListInactiveRequest;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.fragment.AvailableToolsFragment;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvailableToolAdapter extends RecyclerView.Adapter<AvailableToolAdapter.VeiwHolder> {

    View view;
    Context context;

    List<ToolListInactiveRequest> list;
    ToolListInactiveRequest toolsModel;
    SparseBooleanArray itemStateArray = new SparseBooleanArray();
    ApiInterfaceListener listener;
    private AvailableToolsFragment fragment;


    public AvailableToolAdapter(Context context, List<ToolListInactiveRequest> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public VeiwHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.content_tool_list_item, parent, false);
        return new VeiwHolder(view);
    }

    @Override
    public void onBindViewHolder(final VeiwHolder holder, int position) {
        toolsModel = getItem(position);
        holder.title.setText(toolsModel.getName());
        holder.box.setTag(position);
        holder.bind(position, toolsModel);

    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public void Clear() {
        list.clear();
        notifyDataSetChanged();
    }

    public void setFragment(AvailableToolsFragment fragment) {
        this.fragment = fragment;
    }

    public class VeiwHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        CheckBox box;
        ToolListInactiveRequest toolsModel;

        public VeiwHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txtToolName);
            box = itemView.findViewById(R.id.cbTool);
            itemView.setOnClickListener(this);
        }

        public void bind(int position, ToolListInactiveRequest toolsModel) {
            this.toolsModel = toolsModel;
            if (!itemStateArray.get(position, false)) {
                box.setChecked(false);
            } else {
                box.setChecked(true);
            }

        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            if (!itemStateArray.get(adapterPosition, false)) {
                box.setChecked(true);
                itemStateArray.put(adapterPosition, true);
                final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String dateAndTime = format.format(System.currentTimeMillis());
                Log.e("onClick: ", dateAndTime);
                getUpdate(String.valueOf(toolsModel.getId()), dateAndTime);
            } else {
                box.setChecked(false);
                itemStateArray.put(adapterPosition, false);

            }
        }
    }

    public ToolListInactiveRequest getItem(int position) {
        return list.get(position);
    }

    private void getUpdate(String tool_id, String date_time) {
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<SimpleResponse> call = listener.checkInOutTools(user_id, api_token, tool_id, "0", date_time);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                if (response.isSuccessful()) {
                    SimpleResponse simpleResponse = response.body();
                    if (simpleResponse.getStatus().equals("true")) {
                        Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        fragment.UpdateList();
                    } else {
                        Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {

            }
        });
    }

}



