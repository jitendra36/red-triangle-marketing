package com.redtrianglemarketing.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redtrianglemarketing.Model.request.JobCard;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.activity.JobCardDetailActivity;

import java.util.List;

public class ChangeOrderListAdapter extends RecyclerView.Adapter<ChangeOrderListAdapter.ViewHolder> {
    Context mContext;
    List<JobCard> list;
    JobCard mJobCard;


    public ChangeOrderListAdapter(Context mContext, List<JobCard> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.jobcard_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mJobCard=getItem(position);
        holder.textView.setText(mJobCard.getJobTitle());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                Intent intent=new Intent(mContext, JobCardDetailActivity.class);
                intent.putExtra("jobCard_id",mJobCard.getId());
                intent.putExtra("jobCard_cart",mJobCard.getJobId());
                intent.putExtra("title",mJobCard.getJobTitle());
                mContext.startActivity(intent);
            }
        });
    }

    private JobCard getItem(int position) {
        return list.get(position);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        LinearLayout layout;
        public ViewHolder(View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.jobcard);
            layout=itemView.findViewById(R.id.linear);
        }
    }
}
