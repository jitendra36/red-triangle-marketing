package com.redtrianglemarketing.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.redtrianglemarketing.Model.request.JobdetailList;
import com.redtrianglemarketing.R;

import java.util.List;

public class CreateJobAdapter extends RecyclerView.Adapter<CreateJobAdapter.ViewHolder> {
    Context context;
    List<JobdetailList> list;
    JobdetailList mJobdetailList;

    public CreateJobAdapter(Context context, List<JobdetailList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.list_cart_show_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        mJobdetailList=getItem(position);

        holder.name.setText(mJobdetailList.getJobCardName());
        holder.qty.setText(String.valueOf(mJobdetailList.getJobCardQty()));
        holder.prise.setText(String.valueOf(mJobdetailList.getJobCardPrice()));
        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(position);
                grandTotal(list);
                notifyDataSetChanged();
            }
        });
        grandTotal(list);
    }

    private JobdetailList getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    private int grandTotal(List<JobdetailList> items) {

        int totalPrice = 0;
        for (int i = 0; i < items.size(); i++) {
            totalPrice += items.get(i).getJobCardPrice() * items.get(i).getJobCardQty();
        }
//        amount.setText(String.valueOf(totalPrice));
        return totalPrice;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView qty;
        TextView prise;
        ImageButton info;
        public ViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.itemName);
             qty = itemView.findViewById(R.id.ItemQty);
             prise = itemView.findViewById(R.id.ItemPrice);
             info = itemView.findViewById(R.id.imgbtnRemove);
        }
    }
}
