package com.redtrianglemarketing.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.JobfolderList;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.activity.FolderListActivity;

import java.util.List;

public class FolderListAdapter extends RecyclerView.Adapter<FolderListAdapter.ViewHolder> {
    Context mContext;
    List<JobfolderList> list;
    JobfolderList mJobfolderList;


    public FolderListAdapter(Context mContext, List<JobfolderList> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.folder_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mJobfolderList = getItem(position);
        holder.layout.setVisibility(View.VISIBLE);
        holder.textView.setText(mJobfolderList.getFolderName());

        holder.layout.setTag(position);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout ll = (LinearLayout) view;
                if (ll.getTag() == view.getTag()) {
                    Intent intent = new Intent(mContext, FolderListActivity.class);
                    intent.putExtra(AppConstants.RequestDataKey.folder_id, String.valueOf(list.get(Integer.parseInt("" + ll.getTag())).getId()));
                    intent.putExtra(AppConstants.RequestDataKey.job_id, String.valueOf(list.get(Integer.parseInt("" + ll.getTag())).getJobId()));
                    intent.putExtra("title", String.valueOf(list.get(Integer.parseInt("" + ll.getTag())).getFolderName()));
                    mContext.startActivity(intent);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public JobfolderList getItem(int position) {
        return list.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        LinearLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.folder);
            layout = itemView.findViewById(R.id.folder_select);
        }
    }
}
