package com.redtrianglemarketing.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.JobListRequest;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.activity.HomeActivity;

import java.util.List;

public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.ViewHolder> {
    Context context;
    JobListRequest jobList = new JobListRequest();
    List<JobListRequest> list;
    View view;

    public JobListAdapter(Context context, List<JobListRequest> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.job_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        jobList = getItem(position);

        holder.job.setText(jobList.getTitle());

        holder.ll.setTag(position);
        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CardView job = (CardView) view;
                if (job.getTag() == view.getTag()) {
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra(AppConstants.RequestDataKey.id, String.valueOf(list.get(Integer.parseInt(("" + job.getTag()))).getJobId()));
                    intent.putExtra("title", String.valueOf(list.get(Integer.parseInt(("" + job.getTag()))).getTitle()));
                    context.startActivity(intent);
                }

            }
        });
    }

    public void add(List<JobListRequest> items) {
        int previousDataSize = this.list.size();
        this.list.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public JobListRequest getItem(int position) {
        return list.get(position);
    }

    public void updateList(List<JobListRequest> temp) {
        list = temp;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView job;
        CardView ll;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            job = view.findViewById(R.id.job_namne);
            ll = view.findViewById(R.id.linearjob);
            imageView = view.findViewById(R.id.job_icon);
        }
    }

}
