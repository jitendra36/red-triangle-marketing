package com.redtrianglemarketing.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.redtrianglemarketing.Model.request.JobdetailList;
import com.redtrianglemarketing.R;

import java.util.List;

public class ListAdapter extends BaseAdapter {
    Context context;
    List<JobdetailList> list;

    public ListAdapter(Context context, List<JobdetailList> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final JobdetailList jobCard = list.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.jobcard_list_detail, null);
        TextView name = view.findViewById(R.id.itemName);
        TextView qty = view.findViewById(R.id.ItemQty);
        TextView rs = view.findViewById(R.id.ItemPrice);
        name.setText(jobCard.getJobCardName());
        qty.setText(String.valueOf(jobCard.getJobCardQty()));
        rs.setText(String.valueOf(jobCard.getJobCardPrice()));
        return view;
    }
}
