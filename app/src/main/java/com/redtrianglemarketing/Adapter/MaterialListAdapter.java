package com.redtrianglemarketing.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.SubModel.MaterialList;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.activity.MaterialDetailActivity;

import java.util.List;
import java.util.Locale;

public class MaterialListAdapter extends RecyclerView.Adapter<MaterialListAdapter.ViewHolder> {

    List<MaterialList> data;
    MaterialList MaterialList;
    private Context mContext;
    public MaterialListAdapter(List<MaterialList> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.content_material_item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        MaterialList=getItem(position);
        holder.txtMaterial.setText(MaterialList.getMaterialName());
        holder.txtQuantity.setText(String.format(Locale.getDefault(), "%d Qty.",MaterialList.getStock()));
        holder.bind(holder.getAdapterPosition(), String.valueOf(MaterialList.getId()),MaterialList.getMaterialName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public MaterialList getItem(int position) {
        return data.get(position);
    }


    public void add(List<MaterialList> items) {
        int previousDataSize = this.data.size();
        this.data.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txtMaterial;
        public final TextView txtQuantity;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtMaterial = (TextView) view.findViewById(R.id.txtMaterial);
            txtQuantity = (TextView) view.findViewById(R.id.txtQuantity);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtMaterial.getText() + "'";
        }

        public void bind(int position, final String id, final String materialName) {
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e( "onClick: ",id );
                    Intent intent = new Intent(mContext, MaterialDetailActivity.class);
                    intent.putExtra(AppConstants.RequestDataKey.id,id);
                    intent.putExtra("title",materialName );
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
