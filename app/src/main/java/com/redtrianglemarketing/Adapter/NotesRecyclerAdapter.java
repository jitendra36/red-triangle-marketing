package com.redtrianglemarketing.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.NotesRequest;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.fragment.Notes;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.ViewHolder> {
    Context context;
    NotesRequest notesRequest;
    List<NotesRequest> list;
    Dialog dialog;
    View view;
    ApiInterfaceListener listener;
    String user_id;
    String api_token;
    String job_id;
    Notes fragment;


    public NotesRecyclerAdapter(Context context, List<NotesRequest> list, Notes fragment) {
        this.context = context;
        this.list = list;
        this.fragment = fragment;
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.notes_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        notesRequest = getItem(position);

        holder.job.setText(notesRequest.getNotes());

        holder.menu.setTag(position);
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ImageView cardView = (ImageView) view;
                if (cardView.getTag().equals(view.getTag())) {
                    job_id = String.valueOf(list.get(Integer.parseInt("" + cardView.getTag())).getJobId());
                    Context custom = new ContextThemeWrapper(context, R.style.CustomPop);
                    PopupMenu popup = new PopupMenu(custom, holder.menu);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater()
                            .inflate(R.menu.poupup_menu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            switch (menuItem.getItemId()) {
                                case R.id.edit:
                                    if (Network.isConnected(context)) {
                                        editNotes((list.get(Integer.parseInt(("" + cardView.getTag()))).getNotesId()),notesRequest.getNotes());
                                    } else {
                                        Toast.makeText(context, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                case R.id.delete:
                                    int gettoposition = list.indexOf(list.get(Integer.parseInt("" + cardView.getTag())));
                                    if (Network.isConnected(context)) {

                                        deleteNotes((list.get(Integer.parseInt(("" + cardView.getTag()))).getNotesId()), gettoposition);
                                    } else {
                                        Toast.makeText(context, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                            }
                            return true;
                        }
                    });
                    popup.show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public NotesRequest getItem(int position) {
        return list.get(position);
    }

    public void add(List<NotesRequest> items) {

        int previousDataSize = this.list.size();
        this.list.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView job;
        CardView ll;
        ImageView menu;


        public ViewHolder(View itemView) {
            super(itemView);
            job = view.findViewById(R.id.job_namne);
            ll = view.findViewById(R.id.linearjob);
            menu = view.findViewById(R.id.job_icon);
        }
    }

    private void editNotes(final int not_id,String note) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_alert_card);
        final EditText editTitle=dialog.findViewById(R.id._title);
        editTitle.setVisibility(View.GONE);
        final EditText jobTitle = dialog.findViewById(R.id.add_job_title);
        final Button submit = dialog.findViewById(R.id.button);
        final TextView title = dialog.findViewById(R.id.textView8);
        jobTitle.setText(note);
        jobTitle.setSelection(jobTitle.getText().length());
        title.setText(String.format(Locale.getDefault(), "%s", "Edit Notes"));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(jobTitle.getText().toString())) {
                    String notes = jobTitle.getText().toString().trim();
                    if (Network.isConnected(context)) {
                        editNotes(notes, not_id);
                    } else {
                        Toast.makeText(context, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Notes Is Not Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
    }

    private void editNotes(final String notes, int note_id) {

        Call<SimpleResponse> call = listener.editnotes(user_id, api_token, job_id, notes, String.valueOf(note_id));
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    SimpleResponse simpleResponse = response.body();
                    fragment.notesList();
                    notifyDataSetChanged();
                    Toast.makeText(view.getContext(), simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {

            }
        });
    }

    private void deleteNotes(int notes, final int position) {
        Call<SimpleResponse> call = listener.deleteNotes(user_id, api_token, String.valueOf(notes));
        call.enqueue(new Callback<SimpleResponse>() {
            @Override

            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                if (response.isSuccessful()) {
                    SimpleResponse simpleResponse = response.body();
                    list.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, list.size());
                    Toast.makeText(view.getContext(), simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {

            }
        });
    }

}
