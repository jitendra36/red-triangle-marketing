package com.redtrianglemarketing.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.SubModel.JobtaskList_;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.VeiwHolder> {

    private static final String TAG = TaskListAdapter.class.getSimpleName();
    View view;
    Context context;
    JobtaskList_ jobtaskList;
    List<JobtaskList_> list;
    String status = "";
    String jobtask_id;

    ApiInterfaceListener listener;


    public TaskListAdapter(Context context, List<JobtaskList_> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public VeiwHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.check_list, parent, false);
        return new VeiwHolder(view);
    }

    @Override
    public void onBindViewHolder(final VeiwHolder holder, int position) {
        jobtaskList = getItem(position);
        holder.title.setText(jobtaskList.getTitle());

        if (jobtaskList.getJobtaskStatus().equals("done")) {
            (holder).box.setChecked(true);
        } else {
            (holder).box.setChecked(false);
        }


        holder.box.setTag(position);
        holder.box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                CheckBox checkBox = (CheckBox) compoundButton;
                if (checkBox.getTag() == compoundButton.getTag()) {
                    if (holder.box.isChecked()) {
                        System.out.println("Checked");
                        status = "done";
                        jobtask_id = String.valueOf(list.get(Integer.parseInt(("" + checkBox.getTag()))).getId());
                        if (Network.isConnected(context)) {
                            getUpdate(jobtask_id, status);
                        } else {
                            Toast.makeText(context, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        System.out.println("Un-Checked");
                        status = "Pending";
                        jobtask_id = String.valueOf(list.get(Integer.parseInt(("" + checkBox.getTag()))).getId());
                        if (Network.isConnected(context)) {
                            getUpdate(jobtask_id, status);
                        } else {
                            Toast.makeText(context, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VeiwHolder extends RecyclerView.ViewHolder {
        TextView title;
        CheckBox box;
        public VeiwHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            box = itemView.findViewById(R.id.checkBox);

        }


    }



    private void updateTime(String flag,String time) {
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<SimpleResponse> call = listener.checkInOutJob(user_id, api_token, jobtask_id,flag,time);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                SimpleResponse simpleResponse = response.body();
                if (response.body().getStatus().equals("true")) {
                    Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {

            }
        });
    }

    public JobtaskList_ getItem(int position) {
        return list.get(position);
    }

    private void getUpdate(String jobtask_id, String status) {
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<SimpleResponse> call = listener.jobTaskUpdate(user_id, api_token, jobtask_id, status);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                SimpleResponse simpleResponse = response.body();
                if (response.body().getStatus().equals("true")) {
                    Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {

            }
        });
    }

}
