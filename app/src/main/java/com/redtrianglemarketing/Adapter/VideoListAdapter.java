package com.redtrianglemarketing.Adapter;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.Model.request.VideoRequest;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.PermissionUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {
    Context mContext;
    List<VideoRequest> list;
    VideoRequest mVideoRequest;
//    String doc_file = null;
    private ProgressDialog mProgressDialog;
    String video_file = null;

    public VideoListAdapter(Context mContext, List<VideoRequest> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.folder_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mVideoRequest=getItem(position);
        holder.linearLayout.setVisibility(View.VISIBLE);
        holder.textView.setText(mVideoRequest.getTitle());


        holder.linearLayout.setTag(position);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout ll = (LinearLayout) view;
                if (ll.getTag() == view.getTag()) {
//                    try {
//                        video_file = URLDecoder.decode(video_file = list.get(Integer.parseInt("" + ll.getTag())).getVideoUrl(), "UTF-8");
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//                    Intent intent = new Intent(mContext, VideoViewActivity.class);
//                    intent.putExtra("videoUrl", video_file);
//                    intent.putExtra("title", mVideoRequest.getTitle());
//                    mContext.startActivity(intent);

                    if (!PermissionUtil.checkPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        PermissionUtil.requestPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE, 1022);
                    } else if (!PermissionUtil.checkPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        PermissionUtil.requestPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE, 1022);
                    } else {
                        try {
                        video_file = URLDecoder.decode(video_file = list.get(Integer.parseInt("" + ll.getTag())).getVideoUrl(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }                        Log.e("", video_file);
                        if (video_file != null) {
                            mProgressDialog = new ProgressDialog(view.getContext());
                            mProgressDialog.setMessage("Download");
                            mProgressDialog.setIndeterminate(true);
                            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            mProgressDialog.setCancelable(false);
                            String url = video_file;


                            File storagePath1 = new File(Environment.getExternalStorageDirectory() + "/NGN");
                            storagePath1.mkdirs();

                            String str = url.substring(url.lastIndexOf("/") + 1);


                            Uri fileURI = null;

                            File file1 = new File(storagePath1, str);


                            fileURI = FileProvider.getUriForFile(mContext,
                                    mContext.getPackageName() + ".provider",
                                    file1);


                            if (file1.exists()) {
                                Log.e("", video_file);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.setDataAndType(fileURI, "*/*");
                                mContext.startActivity(intent);
                            } else {
                                final DownloadTask DownloadTask = new DownloadTask(view.getContext());
                                DownloadTask.execute(url);
                            }

                            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    //downloadTask.cancel(true);
                                }
                            });
                        }
                    }

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public VideoRequest getItem(int position) {
        return list.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
             textView = itemView.findViewById(R.id.video_list);
             linearLayout = itemView.findViewById(R.id.video_select);
        }
    }
    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());mWakeLock.acquire();
            mProgressDialog.show();
        }


        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            String filename = null;
            File storagePath = new File(Environment.getExternalStorageDirectory() + "/NGN");
            if (storagePath.exists() == false) {
                storagePath.mkdirs();
            }
            String file1 = sUrl[0].substring(sUrl[0].lastIndexOf("/") + 1);
            filename = file1;
            File file = new File(storagePath, filename);
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage();
                }
                int fileLength = connection.getContentLength();

                input = connection.getInputStream();
                output = new FileOutputStream(file);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }
                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null) {
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
                Log.e("Download", result);
            } else {
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
