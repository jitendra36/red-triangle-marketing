package com.redtrianglemarketing.AppConstants;

import android.content.Context;


public class AppConstants
{

    public static Context CONTEXT;
    public static String PREF_FILE="PREF_RSC";
    public static final String logStatus="";



    public class RequestDataKey {
        public static final String name = "name";
        public static final String email = "email";
        public static final String password = "password";
        public static final String status = "status";
        public static final String device_token = "device_token";
        public static final String device_type = "device_type";
        public static final String api_token = "api_token";
        public static final String user_id = "user_id";
        public static final String password_confirmation= "password_confirmation";
        public static final String old_password= "old_password";
        public static final String job_id="job_id";
        public static final String folder_id="folder_id";
        public static final String job_card_id="job_card_id";
        public static final String data="data";
        public static final String job_title="job_title";
        public static final String image="image";
        public static final String second_client_name="second_client_name";
        public static final String id="id";
        public static final String product_id="product_id";
        public static final String emailid="emailid";
        public static final String description="description";
        public static final String jobtask_id="jobtask_id";
        public static final String jobtask_status="jobtask_status";
        public static final String page="page";
        public static final String search="search";
        public static final String notes="notes";
        public static final String note_id="note_id";
        public static final String document_title="document_title";
        public static final String document_description="document_description";
        public static final String video_title="video_title";
        public static final String video_description="video_description";
        public static final String video="video";
        public final static String material_id="material_id";
        public final static String quantity="quantity";
        public static final String tool_id="tool_id";
        public static final String in_out_flag="in_out_flag";
        public static final String date_time="date_time";
        public static final String phone="phone";
        public static final String message="message";
        public static final String note="note";
    }
    public class preDataKey{
        public static final String name = "name";
        public static final String email = "email";
        public static final String api_token = "api_token";
        public static final String user_id = "user_id";
    }

}
