package com.redtrianglemarketing.Model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.redtrianglemarketing.Model.response.LoginResponse;
import com.redtrianglemarketing.Model.response.UserAuthenticated;

import java.lang.reflect.Type;

public class LoginDeserializer implements JsonDeserializer<LoginResponse> {
    @Override
    public LoginResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Log.d("Test/Deserializer", "Using a custom deserializer for Login WS");

        Gson gson = new Gson();
        LoginResponse loginResponse = gson.fromJson(json, LoginResponse.class);

        if (loginResponse.getStatus().equals("true")) {
            // The full response as a json object
            JsonElement jsonElement = json.getAsJsonObject().get("data");

            // The response will be parsed because the status was 1
            UserAuthenticated userAuthenticated = gson.fromJson(jsonElement, UserAuthenticated.class);
            loginResponse.setRequest(userAuthenticated);
        }

        return loginResponse;
    }
}
