package com.redtrianglemarketing.Model;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.redtrianglemarketing.Model.response.ProfileUpdateResponse;
import com.redtrianglemarketing.Model.response.UserAuthenticated;


import java.lang.reflect.Type;

public class ProfilerDeserializer implements JsonDeserializer<ProfileUpdateResponse> {
    @Override
    public ProfileUpdateResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ProfileUpdateResponse updateResponse = gson.fromJson(json, ProfileUpdateResponse.class);

        if (updateResponse.getStatus().equals("true")) {
            // The full response as a json object
            JsonElement jsonElement = json.getAsJsonObject().get("data");

            // The response will be parsed because the status was 1
            UserAuthenticated userAuthenticated = gson.fromJson(jsonElement, UserAuthenticated.class);
            updateResponse.setResponse(userAuthenticated);
        }

        return updateResponse;
    }
}
