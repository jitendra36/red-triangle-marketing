package com.redtrianglemarketing.Model;

public class ToolsModel {
    private int id;
    private String jobtaskStatus,title;

    public String getJobtaskStatus() {
        return jobtaskStatus;
    }

    public void setJobtaskStatus(String jobtaskStatus) {
        this.jobtaskStatus = jobtaskStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
