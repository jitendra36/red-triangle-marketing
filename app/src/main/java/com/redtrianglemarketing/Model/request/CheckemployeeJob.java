package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckemployeeJob {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("jobtask_id")
    @Expose
    private Integer jobtaskId;
    @SerializedName("employee_id")
    @Expose
    private Integer employeeId;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobtaskId() {
        return jobtaskId;
    }

    public void setJobtaskId(Integer jobtaskId) {
        this.jobtaskId = jobtaskId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
