package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DocumentListModel {
    @SerializedName("document_detail")
    @Expose
    private List<DocumentRequest> documentDetail = null;



    public List<DocumentRequest> getDocumentDetail() {
        return documentDetail;
    }

    public void setDocumentDetail(List<DocumentRequest> documentDetail) {
        this.documentDetail = documentDetail;
    }

}
