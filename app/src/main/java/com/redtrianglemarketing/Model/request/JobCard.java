package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobCard {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("job_card_signature_image")
    @Expose
    private String jobCardSignatureImage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("second_client_name")
    @Expose
    private String secondClientName;
    @SerializedName("jobdetail_list")
    @Expose
    private List<JobdetailList> jobdetailList = null;


    public JobCard(String s)
    {
        totalAmount +=s;
        setTotalAmount(totalAmount);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTotalAmount()
    {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getJobCardSignatureImage() {
        return jobCardSignatureImage;
    }

    public void setJobCardSignatureImage(String jobCardSignatureImage) {
        this.jobCardSignatureImage = jobCardSignatureImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSecondClientName() {
        return secondClientName;
    }

    public void setSecondClientName(String secondClientName) {
        this.secondClientName = secondClientName;
    }

    public List<JobdetailList> getJobdetailList() {
        return jobdetailList;
    }

    public void setJobdetailList(List<JobdetailList> jobdetailList) {
        this.jobdetailList = jobdetailList;
    }
}
