package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobCardRequest {


    @SerializedName("job_card")
    @Expose
    private List<JobCard> jobCard = null;

    public List<JobCard> getJobCard() {
        return jobCard;
    }

    public void setJobCard(List<JobCard> jobCard) {
        this.jobCard = jobCard;
    }
}
