package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobListData {

    @SerializedName("current_page")
    
    private Integer currentPage;
    @SerializedName("data")
    
    private List<JobListRequest> data = null;
    @SerializedName("from")
    
    private Integer from;
    @SerializedName("last_page")
    
    private Integer lastPage;
    @SerializedName("next_page_url")
    
    private String nextPageUrl;
    @SerializedName("path")
    
    private String path;
    @SerializedName("per_page")
    
    private Integer perPage;
    @SerializedName("prev_page_url")
    
    private Object prevPageUrl;
    @SerializedName("to")
    
    private Integer to;
    @SerializedName("total")
    
    private Integer total;

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public List<JobListRequest> getData() {
        return data;
    }

    public void setData(List<JobListRequest> data) {
        this.data = data;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Object getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(Object prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
