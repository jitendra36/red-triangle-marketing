package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class JobdetailList extends JSONObject implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("job_card_id")
    @Expose
    private String jobCardId;
    @SerializedName("job_card_name")
    @Expose
    private String jobCardName;
    @SerializedName("job_card_qty")
    @Expose
    private Integer jobCardQty;
    @SerializedName("job_card_price")
    @Expose
    private Integer jobCardPrice;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("job_card_status")
    @Expose
    private String jobCardStatus;
    @SerializedName("status")
    @Expose
    private String status;


    public JobdetailList()
    {
    }
    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", jobCardName);
            obj.put("qty", jobCardQty);
            obj.put("price", jobCardPrice);
        } catch (JSONException e)
        {

        }

        return obj;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getJobCardName() {
        return jobCardName;
    }

    public void setJobCardName(String jobCardName) {
        this.jobCardName = jobCardName;
    }

    public Integer getJobCardQty() {
        return jobCardQty;
    }

    public void setJobCardQty(Integer jobCardQty) {
        this.jobCardQty = jobCardQty;
    }

    public Integer getJobCardPrice() {
        return jobCardPrice;
    }

    public void setJobCardPrice(Integer jobCardPrice) {
        this.jobCardPrice = jobCardPrice;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getJobCardStatus() {
        return jobCardStatus;
    }

    public void setJobCardStatus(String jobCardStatus) {
        this.jobCardStatus = jobCardStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
