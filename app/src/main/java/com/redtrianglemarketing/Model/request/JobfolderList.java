package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobfolderList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("folder_name")
    @Expose
    private String folderName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("folder_img")
    @Expose
    private String folderImg;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("sortno")
    @Expose
    private Object sortno;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getFolderImg() {
        return folderImg;
    }

    public void setFolderImg(String folderImg) {
        this.folderImg = folderImg;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Object getSortno() {
        return sortno;
    }

    public void setSortno(Object sortno) {
        this.sortno = sortno;
    }
}
