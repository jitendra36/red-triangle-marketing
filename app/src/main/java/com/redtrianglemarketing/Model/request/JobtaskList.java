package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.redtrianglemarketing.Model.SubModel.JobtaskList_;


import java.util.List;

public class JobtaskList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("job_employee_id")
    @Expose
    private String jobEmployeeId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("jobtask_list")
    @Expose
    private List<JobtaskList_> jobtaskList = null;
    @SerializedName("checkemployee_job")
    @Expose
    private List<CheckemployeeJob> checkemployeeJob = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getJobEmployeeId() {
        return jobEmployeeId;
    }

    public void setJobEmployeeId(String jobEmployeeId) {
        this.jobEmployeeId = jobEmployeeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<JobtaskList_> getJobtaskList() {
        return jobtaskList;
    }

    public void setJobtaskList(List<JobtaskList_> jobtaskList) {
        this.jobtaskList = jobtaskList;
    }

    public List<CheckemployeeJob> getCheckemployeeJob() {
        return checkemployeeJob;
    }

    public void setCheckemployeeJob(List<CheckemployeeJob> checkemployeeJob) {
        this.checkemployeeJob = checkemployeeJob;
    }
}
