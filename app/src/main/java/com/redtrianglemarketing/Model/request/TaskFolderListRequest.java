package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TaskFolderListRequest {
    @SerializedName("JobtaskList")
    @Expose
    private List<JobtaskList> jobtaskList = null;


//    @SerializedName("folder")
//    @Expose
//    private Folder folder;

    public List<JobtaskList> getJobtaskList() {
        return jobtaskList;
    }

    public void setJobtaskList(List<JobtaskList> jobtaskList) {
        this.jobtaskList = jobtaskList;
    }

//    public Folder getFolder() {
//        return folder;
//    }
//
//    public void setFolder(Folder folder) {
//        this.folder = folder;
//    }
}
