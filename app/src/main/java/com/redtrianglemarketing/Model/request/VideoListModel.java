package com.redtrianglemarketing.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoListModel {
    @SerializedName("video_detail")
    @Expose
    private List<VideoRequest> videoDetail = null;


    public List<VideoRequest> getVideoDetail() {
        return videoDetail;
    }

    public void setVideoDetail(List<VideoRequest> videoDetail) {
        this.videoDetail = videoDetail;
    }

}
