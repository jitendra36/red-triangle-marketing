package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordResponse {

    @SerializedName("status")
    String status;
//    @SerializedName("data")
//    String data;
    @SerializedName("message")
String message;
//    @SerializedName("validation")
//    String validation;
    @SerializedName("code")
String code;

    public ChangePasswordResponse()
    {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

//    public String getData() {
//        return data;
//    }
//
//    public void setData(String data) {
//        this.data = data;
//    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//    public String getValidation() {
//        return validation;
//    }
//
//    public void setValidation(String validation) {
//        this.validation = validation;
//    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
