package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.redtrianglemarketing.Model.request.JobDetailRequest;


import java.util.List;

public class JobDetailResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<JobDetailRequest> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<JobDetailRequest> getData() {
        return data;
    }

    public void setData(List<JobDetailRequest> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
