package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("status")
    String status;
    @SerializedName("message")
    String message;


    public LoginResponse() {
    }

    UserAuthenticated request;

    public UserAuthenticated getRequest() {
        return request;
    }

    public void setRequest(UserAuthenticated request) {
        this.request = request;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}