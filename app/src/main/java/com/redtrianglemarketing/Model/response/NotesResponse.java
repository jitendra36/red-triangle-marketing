package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.SerializedName;
import com.redtrianglemarketing.Model.request.NotesRequest;


import java.util.List;

public class NotesResponse
{
    @SerializedName("status")
    private Boolean status;
    @SerializedName("data")
    private List<NotesRequest> data = null;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private Integer code;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<NotesRequest> getData() {
        return data;
    }

    public void setData(List<NotesRequest> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
