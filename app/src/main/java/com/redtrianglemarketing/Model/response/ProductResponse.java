package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.redtrianglemarketing.Model.request.ProductRequest;


import java.util.List;

public class ProductResponse  {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<List<ProductRequest>> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<List<ProductRequest>> getData() {
        return data;
    }

    public void setData(List<List<ProductRequest>> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
