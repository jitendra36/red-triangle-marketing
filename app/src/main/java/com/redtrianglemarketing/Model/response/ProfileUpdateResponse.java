package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.SerializedName;

public class ProfileUpdateResponse
{

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    UserAuthenticated response;

    public UserAuthenticated getResponse() {
        return response;
    }

    public void setResponse(UserAuthenticated response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
