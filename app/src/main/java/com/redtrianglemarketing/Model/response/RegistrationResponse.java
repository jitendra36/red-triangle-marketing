package com.redtrianglemarketing.Model.response;



import com.redtrianglemarketing.Model.request.LoginRequest;

import java.util.ArrayList;

public class RegistrationResponse
{

    String message;
    String status;
    public ArrayList<LoginRequest> DATA=new ArrayList<>();

    public RegistrationResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<LoginRequest> getDATA() {
        return DATA;
    }

    public void setDATA(ArrayList<LoginRequest> DATA) {
        this.DATA = DATA;
    }
}
