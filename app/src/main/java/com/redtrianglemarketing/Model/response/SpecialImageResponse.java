package com.redtrianglemarketing.Model.response;

public class SpecialImageResponse {
    private String message;

    private Result result;

    private String status;

    private String success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", result = " + result + ", status = " + status + ", success = " + success + "]";
    }

    private class Result {
        private String id;

        private String updated_at;

        private String file_4;

        private String file_3;

        private String created_at;

        private String file_1;

        private String file_2;

        private String note;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getFile_4() {
            return file_4;
        }

        public void setFile_4(String file_4) {
            this.file_4 = file_4;
        }

        public String getFile_3() {
            return file_3;
        }

        public void setFile_3(String file_3) {
            this.file_3 = file_3;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getFile_1() {
            return file_1;
        }

        public void setFile_1(String file_1) {
            this.file_1 = file_1;
        }

        public String getFile_2() {
            return file_2;
        }

        public void setFile_2(String file_2) {
            this.file_2 = file_2;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", updated_at = " + updated_at + ", file_4 = " + file_4 + ", file_3 = " + file_3 + ", created_at = " + created_at + ", file_1 = " + file_1 + ", file_2 = " + file_2 + ", note = " + note + "]";
        }
    }
}
