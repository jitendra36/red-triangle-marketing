package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.redtrianglemarketing.Model.request.TaskRequest;


import java.util.List;

public class TaskResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<TaskRequest> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<TaskRequest> getData() {
        return data;
    }

    public void setData(List<TaskRequest> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
