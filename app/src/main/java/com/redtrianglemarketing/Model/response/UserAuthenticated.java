package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.SerializedName;

public class UserAuthenticated {
    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("email")
    public String email;
    @SerializedName("status")
    public String status;
    @SerializedName("language")
    public String language;
    @SerializedName("deleted_at")
    public String deleted_at;
    @SerializedName("device_token")
    public String device_token;
    @SerializedName("device_type")
    public String device_type;
    @SerializedName("api_token")
    public String api_token;

}
