package com.redtrianglemarketing.Model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.redtrianglemarketing.Model.request.VideoListModel;


public class VideoResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private VideoListModel data;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public VideoListModel getData() {
        return data;
    }

    public void setData(VideoListModel data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
