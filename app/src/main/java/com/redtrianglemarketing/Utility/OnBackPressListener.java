package com.redtrianglemarketing.Utility;

public interface OnBackPressListener {
    boolean onBackPress();
}
