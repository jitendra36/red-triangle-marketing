package com.redtrianglemarketing.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.redtrianglemarketing.AppConstants.AppConstants;


public class Pref {

    private static SharedPreferences sharedPreferences = null;

    public static void openPref()
    {
        sharedPreferences = AppConstants.CONTEXT.getSharedPreferences(AppConstants.PREF_FILE, Context.MODE_PRIVATE);
    }

    public static String getValue(String key, String defaultValue)
    {
        Pref.openPref();
        String result = Pref.sharedPreferences.getString(key, defaultValue);
        Pref.sharedPreferences = null;
        return result;
    }

    public static void setValue(String key, String value)
    {
        Pref.openPref();
        SharedPreferences.Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
        prefsPrivateEditor.putString(key, value);
        prefsPrivateEditor.commit();
        prefsPrivateEditor = null;
        Pref.sharedPreferences = null;
    }
}
