package com.redtrianglemarketing.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.redtrianglemarketing.Model.response.ContactResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.MessageUtil;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;


import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactFormActivity extends AppCompatActivity {


    private static final String TAG = ContactFormActivity.class.getSimpleName();
    @BindView(R.id.btnSpecialImage)
    ImageButton btnSpecialImage;
    @BindView(R.id.layoutSpecialImages)
    LinearLayout layoutSpecialImages;
    @BindView(R.id.edtFirstName)
    EditText edtFirstName;
    @BindView(R.id.edtLastName)
    EditText edtLastName;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtMessage)
    EditText edtMessage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.btnLogin)
    Button txtLogin;
    @BindView(R.id.txtSpecials)
    TextView txtSpecials;
    private ApiInterfaceListener mApiInterfaceListener;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_form);
        ButterKnife.bind(this);
        mApiInterfaceListener = APIClient.getClient().create(ApiInterfaceListener.class);
        initDialog();
        txtSpecials.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    @OnClick({R.id.layoutSpecialImages, R.id.btnSubmit, R.id.btnLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutSpecialImages:
                startActivity(new Intent(this, SpecialImagesActivity.class));
                break;
            case R.id.btnSubmit:
                String name = edtFirstName.getText().toString().trim();
                String email = edtEmail.getText().toString().trim();
                String phone = edtLastName.getText().toString().trim();

                String message = edtMessage.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    edtFirstName.requestFocus();
                    edtFirstName.setError("Enter Name");
                } else if (TextUtils.isEmpty(email)) {
                    edtEmail.requestFocus();
                    edtEmail.setError("Enter Email");

                } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    edtEmail.requestFocus();
                    edtEmail.setError("Enter Valid Email");

                } else if (TextUtils.isEmpty(phone)) {
                    edtLastName.requestFocus();
                    edtLastName.setError("Enter Contact No");

                } else if (!Patterns.PHONE.matcher(phone).matches()) {
                    edtLastName.requestFocus();
                    edtLastName.setError("Enter Valid Contact No");

                }else if (phone.length()<5) {
                    edtLastName.requestFocus();
                    edtLastName.setError("Enter Valid Contact No");

                }else if (TextUtils.isEmpty(message)) {
                    edtMessage.requestFocus();
                    edtMessage.setError("Enter Message");

                } else {
                    edtFirstName.setError(null);
                    edtLastName.setError(null);
                    edtEmail.setError(null);
                    edtMessage.setError(null);
                    postContact(name, phone, email, message);
                }

                break;
            case R.id.btnLogin:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }

    private void postContact(String name, String phone, String email, String message) {
        mProgressDialog.show();
        mApiInterfaceListener.doContactUs(name, email, phone, message).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    ContactResponse contactResponse = new Gson().fromJson(response.body(), ContactResponse.class);
                    switch (contactResponse.getStatus()) {
                        case "200":
                            clearFeilds();
                            MessageUtil.showToast(ContactFormActivity.this, contactResponse.getMessage());
                            break;
                        default:
                            MessageUtil.showToast(ContactFormActivity.this, contactResponse.getMessage());
                            break;
                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                mProgressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void clearFeilds() {
        edtFirstName.setText("");
        edtEmail.setText("");
        edtLastName.setText("");
        edtMessage.setText("");
    }
}
