package com.redtrianglemarketing.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.JobdetailList;
import com.redtrianglemarketing.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CreateJobCardActivity extends AppCompatActivity {

    String jobTitle;
    String job_id;
     ProgressDialog mProgressDialog;
    ListView jobList;
    EditText name, qty, prise;
    TextView amount;
    Button submit;
    ImageButton fab;
    List<JobdetailList> dataModels;
    private MyListAdapter adapter;
    JobdetailList demo;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_job_card);
        jobList = findViewById(R.id.listItemJob);
        name = findViewById(R.id.editText);
        qty = findViewById(R.id.editText3);
        prise = findViewById(R.id.editText2);
        amount = findViewById(R.id.amount);
        fab = findViewById(R.id.adding);
        submit = findViewById(R.id.create_btn);


        dataModels = new ArrayList<>();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().getExtras() != null) {
            jobTitle = getIntent().getStringExtra(AppConstants.RequestDataKey.job_title);
            job_id = getIntent().getStringExtra(AppConstants.RequestDataKey.job_id);
            getSupportActionBar().setTitle(jobTitle);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String itemName = name.getText().toString();
                String itemQty = qty.getText().toString();
                String itemPrice = prise.getText().toString();
                if (TextUtils.isEmpty(itemName)) {
                    name.setError("Field not empty");
                } else if (TextUtils.isEmpty(itemQty)) {
                    qty.setError("Field not empty");
                } else if (TextUtils.isEmpty(itemPrice)) {
                    prise.setError("Field not empty");
                } else {
                    demo= new JobdetailList();
                    demo.setJobCardName(itemName);
                    demo.setJobCardQty(Integer.valueOf(itemQty));
                    demo.setJobCardPrice(Integer.valueOf(itemPrice));

                    dataModels.add(demo);
                    adapter = new MyListAdapter(getApplicationContext(), dataModels);
                    jobList.setVisibility(View.VISIBLE);
                    jobList.setAdapter(adapter);
                    name.setText("");
                    qty.setText("");
                    prise.setText("");
                    name.requestFocus();
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (demo!=null)
                {
                    Intent i = new Intent(CreateJobCardActivity.this, SignatureActivity.class);
                    i.putExtra(AppConstants.RequestDataKey.job_title, jobTitle);
                    i.putExtra(AppConstants.RequestDataKey.job_id, job_id);
                    i.putExtra("total", amount.getText().toString());
                    i.putExtra("data", (Serializable) adapter.getList());
                    startActivity(i);
                    finish();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Enter at least 1 item", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private class MyListAdapter extends BaseAdapter {

        Context context;
        List<JobdetailList> list;

        public MyListAdapter(Context context, List<JobdetailList> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final JobdetailList folder = list.get(position);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_cart_show_item, null);
            TextView name = convertView.findViewById(R.id.itemName);
            TextView qty = convertView.findViewById(R.id.ItemQty);
            TextView prise = convertView.findViewById(R.id.ItemPrice);
            ImageButton info = convertView.findViewById(R.id.imgbtnRemove);

            name.setText(folder.getJobCardName());
            qty.setText(String.valueOf(folder.getJobCardQty()));
            prise.setText(String.valueOf(folder.getJobCardPrice()));

            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    list.remove(position);
                    grandTotal(list);
                    notifyDataSetChanged();
                }
            });
            grandTotal(list);
            return convertView;
        }

        private int grandTotal(List<JobdetailList> items) {

            int totalPrice = 0;
            for (int i = 0; i < items.size(); i++) {
                totalPrice += items.get(i).getJobCardPrice() * items.get(i).getJobCardQty();
            }
            amount.setText(String.valueOf(totalPrice));
            return totalPrice;
        }


        public List<JobdetailList> getList() {
            return list;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
