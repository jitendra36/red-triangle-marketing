package com.redtrianglemarketing.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.fragment.DocumentList;
import com.redtrianglemarketing.fragment.VideoList;
import java.util.ArrayList;
import java.util.List;

public class FolderListActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ProgressDialog mProgressDialog;
    String job_id;
    String folder_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder_list);
        toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().getExtras()!=null)
        {
            job_id=getIntent().getStringExtra(AppConstants.RequestDataKey.job_id);
            folder_id=getIntent().getStringExtra(AppConstants.RequestDataKey.folder_id);
            String title=getIntent().getStringExtra("title");
            getSupportActionBar().setTitle(title);
        }

        viewPager =findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout =findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        initDialog();

    }
    public String getJobId()
    {
        return job_id ;
    }
    public String getFolderId()
    {
        return folder_id ;
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter=new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DocumentList(),"Document List");
        adapter.addFragment(new VideoList(),"Video List");
        viewPager.setAdapter(adapter);
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
