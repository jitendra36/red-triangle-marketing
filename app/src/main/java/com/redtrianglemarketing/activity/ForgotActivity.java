package com.redtrianglemarketing.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener{
    TextView login;
    EditText mail;
    Button submit;
    ApiInterfaceListener listener;
    ProgressBar progressbar;
    ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        mail=findViewById(R.id.frgt_mail);
        submit=findViewById(R.id.sub_btn);
        login=findViewById(R.id.logintxt);

        progressbar=findViewById(R.id.progressBar);

        listener= APIClient.getClient().create(ApiInterfaceListener.class);

        submit.setOnClickListener(this);
        login.setOnClickListener(this);

    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.logintxt:
                Intent Forgot=new Intent(ForgotActivity.this,LoginActivity.class);
                finish();
                startActivity(Forgot);
                break;
            case R.id.sub_btn:
                if (Network.isConnected(ForgotActivity.this)) {
                    if (validation() == true) {
                        forgotPassword();
                    }
                } else {
                    Toast.makeText(ForgotActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }

    private void forgotPassword()
    {

        String frgt_mail=mail.getText().toString();
        progressbar.setVisibility(View.VISIBLE);
        Call<SimpleResponse> call=listener.forgotPwd(frgt_mail);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                if (response.body()!=null && response.body().getStatus().equals("true"))
                {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(ForgotActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    Intent intent=new Intent(ForgotActivity.this,LoginActivity.class);
                    startActivity(intent);
                }
                else
                {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(ForgotActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                progressbar.setVisibility(View.GONE);
                Toast.makeText(ForgotActivity.this, "Login fail", Toast.LENGTH_LONG).show();
            }
        });
    }


    private boolean validation() {
        boolean value = false;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (!(mail.getText().toString().trim().matches(emailPattern) && mail.getText().toString().trim().length() > 0)) {
            mail.requestFocus();
            mail.setError("Invalid Email");
            value = false;
        }  else {
            value = true;
        }
        return value;
    }
}

