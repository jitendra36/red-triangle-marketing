package com.redtrianglemarketing.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.ListAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.Carddetail;
import com.redtrianglemarketing.Model.request.JobCard;
import com.redtrianglemarketing.Model.request.JobdetailList;
import com.redtrianglemarketing.Model.response.CardDetailResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobCardDetailActivity extends AppCompatActivity {
    private static final String TAG = JobCardDetailActivity.class.getSimpleName();
    TextView amount;
    ListView list;

    ApiInterfaceListener listener;
    Context context;
    String jobId, jobCardId;
    Toolbar toolbar;
    ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_card_detail);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        amount = findViewById(R.id.amount);
        list = findViewById(R.id.listItem);

        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        context = getApplicationContext();
        AppConstants.CONTEXT = context;

        Intent i = getIntent();
        if (i != null) {
            jobCardId = i.getStringExtra("jobCard_id");
            jobId = i.getStringExtra("jobCard_cart");
            String title=getIntent().getStringExtra("title");
            getSupportActionBar().setTitle(title);
        }
        if (Network.isConnected(JobCardDetailActivity.this)) {
            getJobCard();
        } else {
            Toast.makeText(JobCardDetailActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }


    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void getJobCard() {
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<CardDetailResponse> call = listener.jobCardDetail(user_id, api_token, jobId, jobCardId);
        call.enqueue(new Callback<CardDetailResponse>() {
            @Override
            public void onResponse(Call<CardDetailResponse> call, Response<CardDetailResponse> response) {
                if (response.isSuccessful()){
                    CardDetailResponse cardResponse = response.body();

                    Carddetail request = cardResponse.getData();
                    List<JobCard> cardList = request.getJobCard();

                    for (int i = 0; i < cardList.size(); i++) {
                        JobCard card = cardList.get(i);
                        amount.setText(card.getTotalAmount());
                        List<JobdetailList> lists = card.getJobdetailList();
                        SetAdapter(lists);
                    }
                }
                else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }


            }

            @Override
            public void onFailure(Call<CardDetailResponse> call, Throwable t)
            {
                Log.e("cardResponse", t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void SetAdapter(List<JobdetailList> lists) {
        if (lists.size() > 0) {
            ListAdapter adapter = new ListAdapter(this, lists);
            list.setAdapter(adapter);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
