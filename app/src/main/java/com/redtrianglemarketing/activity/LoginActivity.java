package com.redtrianglemarketing.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.LoginRequest;
import com.redtrianglemarketing.Model.response.LoginResponse;
import com.redtrianglemarketing.Model.response.UserAuthenticated;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText user, pass;
    Button login;
    TextView rgst, frgt,txtContact;
    ProgressBar progressbar;
    LoginRequest request;
    Context context;
    ApiInterfaceListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user = findViewById(R.id.userName);
        pass = findViewById(R.id.password);
        login = findViewById(R.id.angry_btn);
        rgst = findViewById(R.id.rgst);
        frgt = findViewById(R.id.frgt);
        txtContact = findViewById(R.id.txtContactUs);
        progressbar = findViewById(R.id.progressBar);
        context = this;
        AppConstants.CONTEXT = context;
        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        login.setOnClickListener(this);
        rgst.setOnClickListener(this);
        frgt.setOnClickListener(this);
        txtContact.setOnClickListener(this);
        request = new LoginRequest();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.angry_btn:
                if (Network.isConnected(LoginActivity.this)) {
                    userLogin();
                } else {
                    Toast.makeText(this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.rgst:
                if (Network.isConnected(LoginActivity.this)) {
                    Intent Register = new Intent(LoginActivity.this, RegisterActivity.class);
                    finish();
                    startActivity(Register);
                } else {
                    Toast.makeText(this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.frgt:
                if (Network.isConnected(LoginActivity.this)) {
                    Intent Forgot = new Intent(LoginActivity.this, ForgotActivity.class);
                    finish();
                    startActivity(Forgot);
                } else {
                    Toast.makeText(this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txtContactUs:
                Intent Forgot = new Intent(LoginActivity.this, ContactFormActivity.class);
                startActivity(Forgot);
                break;
        }
    }

    public void userLogin() {
        if (validation() == true)
        {
            String username = user.getText().toString();
            String password = pass.getText().toString();
//            String username = " snehal.citrusbug@gmail.com";
//            String password = "123456";
            String device_token = "1234568";
            String device_type = "android";
            progressbar.setVisibility(View.VISIBLE);
            retrofit2.Call<LoginResponse> call = listener.loginUser(username, password, device_token, device_type);

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(retrofit2.Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("Response", response.body().toString());

                    if (response.body() != null && response.body().getStatus().equals("true")) {
                        UserAuthenticated authenticated = response.body().getRequest();

                        Pref.setValue(AppConstants.preDataKey.api_token, authenticated.api_token);
                        Pref.setValue(AppConstants.preDataKey.user_id, authenticated.id);
                        Pref.setValue(AppConstants.preDataKey.name, authenticated.name);
                        Pref.setValue(AppConstants.preDataKey.email, authenticated.email);
                        Pref.setValue(AppConstants.logStatus, "true");


                        Log.e("api_token", authenticated.api_token);
                        Log.e("user_id", authenticated.id);

                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("data", response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<LoginResponse> call, Throwable t) {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                    Log.e("data", t.toString());
                }
            });
        }
        else
            {

        }
    }

    private boolean validation() {
        boolean value = true;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (!(user.getText().toString().trim().matches(emailPattern) && user.getText().toString().trim().length() > 0)) {

            user.requestFocus();
            user.setError("Invalid Email");


            value = false;
        } else if (!(pass.getText().toString().trim().length() >= 6)) {

            pass.requestFocus();
            pass.setError("Invalid Password");

            value = false;
        } else {
            value = true;
        }
        return value;
    }


}
