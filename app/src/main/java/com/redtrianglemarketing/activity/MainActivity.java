package com.redtrianglemarketing.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.response.LogoutResponce;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.fragment.AccountFragment;
import com.redtrianglemarketing.fragment.ChangePasswordFragment;
import com.redtrianglemarketing.fragment.HomeFragment;
import com.redtrianglemarketing.fragment.MaterialListFragment;
import com.redtrianglemarketing.fragment.ToolsFragment;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private Handler mHandler;
    public static int navItemIndex = 0;
    ApiInterfaceListener listener;
    private String[] activityTitles;
    private static final String TAG_HOME = "home";

    private static final String TAG_MATERIAL_LIST = "Material List";
    private static final String TAG_TOOLS_LIST = "Tools List";
    private static final String TAG_ACCOUNT = "account";
    private static final String TAG_CHANGE_PASSWORD = "change password";
    public static String CURRENT_TAG = TAG_HOME;

    private View navHeader;
    TextView txtName;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        mHandler = new Handler();
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        context = this;
        AppConstants.CONTEXT = context;
        navHeader = nvDrawer.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        loadNavHeader();
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
    }

    private void loadNavHeader() {
        String name = Pref.getValue(AppConstants.preDataKey.name, "");
        txtName.setText(name);

    }

    private void loadHomeFragment() {
        selectNavMenu();
        setToolbarTitle();
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            mDrawer.closeDrawers();
            return;
        }
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.flContent, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
        mDrawer.closeDrawers();
        invalidateOptionsMenu();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawer.isDrawerOpen(Gravity.START)) {
                    mDrawer.closeDrawers();
                } else {
                    mDrawer.openDrawer(Gravity.START);
                    loadNavHeader();
                }
            }
        });
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // homeFragment
                return new HomeFragment();

            case 1:
                // accountFragment
                return new AccountFragment();
            case 2:
                // changePasswordFragment
                return new ChangePasswordFragment();
            case 3:
                // changePasswordFragment
                return new MaterialListFragment();
            case 4:
                // changePasswordFragment
                return new ToolsFragment();
            default:
                return new HomeFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        nvDrawer.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        nvDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_fragment:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.account_fragment:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_ACCOUNT;
                        break;
                    case R.id.change_password_fragment:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_CHANGE_PASSWORD;
                        break;
                    case R.id.material_fragment:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_MATERIAL_LIST;
                        break;
                    case R.id.tools_fragment:
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_TOOLS_LIST;
                        break;
                    case R.id.logout_fragment:
                        if (Network.isConnected(MainActivity.this)) {
                            callLogout();
                        } else {
                            Toast.makeText(MainActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        navItemIndex = 0;
                        break;
                }
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.START)) {
            mDrawer.closeDrawer(Gravity.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please press back again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 1000);
        }

    }

    private void callLogout() {

        String api_token = Pref.getValue(AppConstants.RequestDataKey.api_token, "");
        String user_id = Pref.getValue(AppConstants.RequestDataKey.user_id, "");

        Call<LogoutResponce> call = listener.logout(api_token, user_id);
        call.enqueue(new Callback<LogoutResponce>() {
            @Override
            public void onResponse(Call<LogoutResponce> call, Response<LogoutResponce> response) {

                Log.e("LogoutResponce", response.toString());
                if (response.body() != null && response.body().getStatus().equals("true")) {
                    Toast.makeText(MainActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    Pref.setValue(AppConstants.preDataKey.user_id, "");
                    Pref.setValue(AppConstants.preDataKey.api_token, "");
                    Pref.setValue(AppConstants.preDataKey.name, "");
                    Pref.setValue(AppConstants.preDataKey.email, "");
                    Pref.setValue(AppConstants.logStatus, "");

                    logout();
                } else {
                    Pref.setValue(AppConstants.preDataKey.user_id, "");
                    Pref.setValue(AppConstants.preDataKey.api_token, "");
                    Pref.setValue(AppConstants.preDataKey.name, "");
                    Pref.setValue(AppConstants.preDataKey.email, "");
                    Pref.setValue(AppConstants.logStatus, "");

                    logout();
                    Toast.makeText(MainActivity.this, "session expire please relogin  ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LogoutResponce> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Logout fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void logout() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("finish", true); // if you are checking for this in your other Activities
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
