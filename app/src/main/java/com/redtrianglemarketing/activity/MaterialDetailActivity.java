package com.redtrianglemarketing.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.MaterialdetailRequest;
import com.redtrianglemarketing.Model.response.MaterialDetailResponse;
import com.redtrianglemarketing.Model.response.StockUpdateResponce;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtMaterial)
    TextView txtMaterial;
    @BindView(R.id.txtQuantity)
    TextView txtQuantity;
    @BindView(R.id.layoutDetail)
    LinearLayout layoutDetail;
    @BindView(R.id.edtQuantity)
    TextInputEditText edtQuantity;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    String id;
    int totalStock;
    ApiInterfaceListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        initActionBar();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            id = i.getStringExtra(AppConstants.RequestDataKey.id);
            String title = i.getStringExtra("title");
            getSupportActionBar().setTitle(title);

            if (Network.isConnected(MaterialDetailActivity.this)) {
                loadDetail(id);
            } else {
                Toast.makeText(MaterialDetailActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @SuppressLint("RestrictedApi")
    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDefaultDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        String stock = edtQuantity.getText().toString().trim();
        if (!stock.equals("")) {
            int addStock = Integer.parseInt(stock);
            if (totalStock >= addStock) {
                updateStack(id, String.valueOf(addStock));
            } else {
                Toast.makeText(MaterialDetailActivity.this, "Enter less than total stock", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(MaterialDetailActivity.this, "Please Enter stock", Toast.LENGTH_SHORT).show();

        }

    }

    private void updateStack(String mt_id, String stc) {
        edtQuantity.setText("");
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<StockUpdateResponce> call = listener.getMaterialUsed(user_id, api_token, mt_id, stc);
        call.enqueue(new Callback<StockUpdateResponce>() {
            @Override
            public void onResponse(Call<StockUpdateResponce> call, Response<StockUpdateResponce> response) {

                StockUpdateResponce detailResponse = response.body();
                if (detailResponse != null) {

                    txtQuantity.setText(String.valueOf(detailResponse.getData()));
                    totalStock = detailResponse.getData();
                    Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    loadDetail(id);
                } else {
                    Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StockUpdateResponce> call, Throwable t) {

            }
        });

    }

    private void loadDetail(String id) {

        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<MaterialDetailResponse> call = listener.materialDetail(user_id, api_token, id);
        call.enqueue(new Callback<MaterialDetailResponse>() {
            @Override
            public void onResponse(Call<MaterialDetailResponse> call, Response<MaterialDetailResponse> response) {

                MaterialDetailResponse detailResponse = response.body();
                if (detailResponse != null) {
                    MaterialdetailRequest request = detailResponse.getData();

                    txtMaterial.setText(request.getMaterialName());
                    txtQuantity.setText(String.valueOf(request.getStock()));
                    totalStock = request.getStock();
//                    Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MaterialDetailResponse> call, Throwable t) {

            }
        });

    }
}
