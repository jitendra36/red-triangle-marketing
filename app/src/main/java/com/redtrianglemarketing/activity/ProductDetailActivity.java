package com.redtrianglemarketing.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.ProductRequest;
import com.redtrianglemarketing.Model.response.ProductDetailResponse;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {

    TextView name, des;
    Button inq;
    ImageView img;
    private Toolbar toolbar;
    ProgressDialog mProgressDialog;
    String product_id = "";
    Dialog dialog;
    ApiInterfaceListener listener;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        componentInitial();

        context = getApplicationContext();
        AppConstants.CONTEXT = context;
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            product_id = intent.getStringExtra(AppConstants.RequestDataKey.product_id);
            if (Network.isConnected(ProductDetailActivity.this)) {
                getDetailProduct();
            } else {
                Toast.makeText(ProductDetailActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
            }

        }


    }

    private void componentInitial() {
        name = findViewById(R.id.pro_name);
        des = findViewById(R.id.pro_dec);
        inq = findViewById(R.id.pro_inq);
        img = findViewById(R.id.pro_imag);

        inq.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pro_inq:
                if (Network.isConnected(ProductDetailActivity.this)) {
                    getInquiry();
                } else {
                    Toast.makeText(ProductDetailActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
                Log.e("done", "done");
                break;
        }
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void getInquiry() {
        dialog = new Dialog(ProductDetailActivity.this);
        dialog.setContentView(R.layout.custom_inquiry);
        final EditText uname = dialog.findViewById(R.id.inq_name);
        final EditText umail = dialog.findViewById(R.id.inq_mail);
        final EditText udesc = dialog.findViewById(R.id.inq_desc);
        final Button submit = dialog.findViewById(R.id.inq_btn);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = uname.getText().toString().trim();
                String userMail = umail.getText().toString().trim();
                String userDesc = udesc.getText().toString().trim();
                if (Network.isConnected(ProductDetailActivity.this)) {
                    getProductInquiry(userName, userMail, userDesc);
                } else {
                    Toast.makeText(ProductDetailActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
    }

    private void getDetailProduct() {
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        Call<ProductDetailResponse> call = listener.productDetail(user_id, api_token, product_id);

        call.enqueue(new Callback<ProductDetailResponse>() {
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ProductDetailResponse detailResponse = response.body();
                    if (detailResponse != null) {
                        List<ProductRequest> request = detailResponse.getData();

                        for (int i = 0; i < request.size(); i++) {
                            ProductRequest pro = request.get(i);
                            Glide.with(context)
                                    .load(pro.getImage())
                                    .apply(new RequestOptions().placeholder(R.mipmap.placeholder))

                                    .into(img);
                            name.setText(pro.getName());
                            setTitle(pro.getName());
                            des.setText(pro.getDescription());
                        }
                    }
                } else {
                    try {
                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable t) {

            }
        });
    }

    private void getProductInquiry(String uname, String emailid, String desc) {
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        Call<SimpleResponse> call = listener.productInquiry(user_id, api_token, product_id, uname, emailid, desc);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                dialog.dismiss();
                SimpleResponse simpleResponse = response.body();
                if (response.body().getStatus().equals("true")) {
                    finish();
                    Toast.makeText(getApplicationContext(), simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                dialog.dismiss();
                finish();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
