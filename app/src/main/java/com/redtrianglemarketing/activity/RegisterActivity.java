package com.redtrianglemarketing.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.Model.response.RegistrationResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    EditText fname, lName, email, pass, conf_pass;
    Button submit;
    TextView lg;
    String firstName, lastName;
    ProgressDialog mProgressDialog;
    ApiInterfaceListener apiInterface;
    ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        fname = findViewById(R.id.fName);
        lName = findViewById(R.id.lNamer);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.password);
        conf_pass = findViewById(R.id.conf_password);
        submit = findViewById(R.id.submit_btn);
        lg = findViewById(R.id.rgst);
        progressbar = findViewById(R.id.progressBar);


        apiInterface = APIClient.getClient().create(ApiInterfaceListener.class);


        submit.setOnClickListener(this);
        lg.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.submit_btn:
                if (Network.isConnected(RegisterActivity.this)) {
                    if (validation() == true) {
                        createUser();
                    }

                } else {
                    Toast.makeText(RegisterActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.rgst:
                Intent Forgot = new Intent(RegisterActivity.this, LoginActivity.class);
                finish();
                startActivity(Forgot);
                break;

        }
    }

    private void createUser() {

        firstName = fname.getText().toString();
        lastName = lName.getText().toString();
        String pswd = pass.getText().toString();
        String mail = email.getText().toString();
        String device_token = "1234568";
        String device_type = "android";
        String status = "active";
        progressbar.setVisibility(View.VISIBLE);
        Call<RegistrationResponse> call = apiInterface.createUser(firstName + " " + lastName, mail, pswd, status, device_token, device_type);
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                if (response.body().getStatus().equalsIgnoreCase("true")) {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                progressbar.setVisibility(View.GONE);
                Toast.makeText(RegisterActivity.this, "Login fail", Toast.LENGTH_LONG).show();

            }
        });
    }



    private boolean validation() {
        boolean value = true;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (fname.getText().toString().equalsIgnoreCase("")) {
            fname.requestFocus();
            fname.setError("Enter first name");
            value = false;
        } else if (lName.getText().toString().equalsIgnoreCase("")) {
            lName.requestFocus();
            lName.setError("Enter last name");
            value = false;
        } else if (!(email.getText().toString().trim().matches(emailPattern) && email.getText().toString().trim().length() > 0)) {
            email.requestFocus();
            email.setError("Invalid Email");
            value = false;
        } else if (!(pass.getText().toString().trim().length() >= 6)) {
            pass.requestFocus();
            pass.setError("minimum six digit required");
            value = false;
        } else if (pass.getText().toString().trim().equalsIgnoreCase("")) {
            conf_pass.requestFocus();
            conf_pass.setError("Enter conform password");
            value = false;
        }else if (!(pass.getText().toString().trim().equals(conf_pass.getText().toString().trim()))) {
            conf_pass.requestFocus();
            conf_pass.setError("Password does not match");
            value = false;
        } else {
            value = true;
        }
        return value;
    }
}

