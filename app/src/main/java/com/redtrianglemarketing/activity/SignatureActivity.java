package com.redtrianglemarketing.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kyanogen.signatureview.SignatureView;
import com.redtrianglemarketing.Adapter.ListAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.JobdetailList;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.PermissionUtil;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignatureActivity extends AppCompatActivity {
    ApiInterfaceListener listener;
    Context context;
    EditText client;
    TextView amount;
    String jobTitle = "", clientName = "";
    String job_id;
    ListView listView;
    List<JobdetailList> lists;
    Bitmap bitmap;
    Button clear, save;
    SignatureView signatureView;
    String path;
    ProgressDialog mProgressDialog;
    JSONObject jObject;

    String data;
    private static final String IMAGE_DIRECTORY = "/signdemo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        amount = findViewById(R.id.amount);
        signatureView = findViewById(R.id.signature_view);
        clear = findViewById(R.id.clear);
        save = findViewById(R.id.save);
        client = findViewById(R.id.clientName);

        listView = findViewById(R.id.listItem);

        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        context = getApplicationContext();
        AppConstants.CONTEXT = context;


        Intent intent = getIntent();

        if (intent.getExtras() != null) {
            jobTitle = getIntent().getStringExtra(AppConstants.RequestDataKey.job_title);
            job_id = getIntent().getStringExtra(AppConstants.RequestDataKey.job_id);
            lists = (List<JobdetailList>) intent.getSerializableExtra("data");
            getSupportActionBar().setTitle(jobTitle);
            String total = getIntent().getStringExtra("total");
            amount.setText(total);

        }
        initDialog();
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clearCanvas();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PermissionUtil.checkPermission(SignatureActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    PermissionUtil.requestPermission(SignatureActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, 1022);
                } else if (!PermissionUtil.checkPermission(SignatureActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    PermissionUtil.requestPermission(SignatureActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, 1022);
                } else {
                    if (Network.isConnected(SignatureActivity.this)) {
                        saveData();
                    } else {
                        Toast.makeText(SignatureActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        JsonConvertData();
        if (Network.isConnected(SignatureActivity.this)) {
            ListItem();
        } else {
            Toast.makeText(SignatureActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1022:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveData();
                }
                break;
        }
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    public void saveData() {
        JsonConvertData();
        String clt = client.getText().toString().trim();
        bitmap = signatureView.getSignatureBitmap();

        path = saveImage(bitmap);
        if (TextUtils.isEmpty(clt)) {
            Toast.makeText(getApplicationContext(), "Enter client name", Toast.LENGTH_SHORT).show();
        } else if (path == null || bitmap==null || signatureView.isBitmapEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter Signature", Toast.LENGTH_SHORT).show();
        } else {
            createJobCard(path, clt);
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY /*iDyme folder*/);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, jobTitle + ".png");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(SignatureActivity.this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";

    }

    private void JsonConvertData() {
        if (lists != null) {
            jObject = new JSONObject();
            try {
                JSONArray jArray = new JSONArray();
                for (JobdetailList jobdetailList : lists) {
                    JSONObject object = new JSONObject();
                    object.put("name", jobdetailList.getJobCardName());
                    object.put("qty", String.valueOf(jobdetailList.getJobCardQty()));
                    object.put("price", String.valueOf(jobdetailList.getJobCardPrice()));
                    jArray.put(object);
                }
                jObject.put("data", jArray);
                data = String.valueOf(jObject);
                Log.e("jObject", jObject.toString());
            } catch (JSONException jse) {
                jse.getStackTrace();
            }
        }
    }

    private void ListItem() {
        if (lists != null) {
            ListAdapter adapter = new ListAdapter(this, lists);
            listView.setAdapter(adapter);
        }
    }

    private void createJobCard(String path, String clt) {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        File file = new File(path);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);


        Call<SimpleResponse> call = listener.createjobcard(user_id, api_token, job_id, String.valueOf(jObject), jobTitle, fileToUpload, clt);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                mProgressDialog.dismiss();
                if (response.body().getStatus().equals("true")) {
                    SimpleResponse simpleResponse = response.body();
                    LocalBroadcastManager.getInstance(SignatureActivity.this).sendBroadcast(new Intent("com.mjmmechanical.ACTION_UPDATE"));
                    finish();
                    Toast.makeText(getApplicationContext(), simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
