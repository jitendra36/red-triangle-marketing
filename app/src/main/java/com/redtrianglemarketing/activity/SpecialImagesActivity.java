package com.redtrianglemarketing.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.redtrianglemarketing.Model.response.SpecialImageResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.MessageUtil;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.PermissionUtil;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SpecialImagesActivity extends AppCompatActivity {

    private static final String TAG = SpecialImagesActivity.class.getSimpleName();
    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.btnUpload1)
    Button btnUpload1;
    @BindView(R.id.img2)
    ImageView img2;
    @BindView(R.id.btnUpload2)
    Button btnUpload2;
    @BindView(R.id.img3)
    ImageView img3;
    @BindView(R.id.btnUpload3)
    Button btnUpload3;
    @BindView(R.id.img4)
    ImageView img4;
    @BindView(R.id.btnUpload4)
    Button btnUpload4;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.edtNotes)
    EditText edtNotes;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    private int pos = 0;
    private ArrayList<String> imagePaths;
    private MultipartBody.Part[] surveyImagesParts;
    private ApiInterfaceListener mApiInterfaceListener;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_images);
        ButterKnife.bind(this);
        mApiInterfaceListener = APIClient.getClient().create(ApiInterfaceListener.class);

        initActionBar();
        initDialog();

        imagePaths = new ArrayList<>();
        surveyImagesParts = new MultipartBody.Part[4];

    }

    @SuppressLint("RestrictedApi")
    private void initActionBar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Must Specials");
        }

    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    @OnClick({R.id.btnUpload1, R.id.btnUpload2, R.id.btnUpload3, R.id.btnUpload4, R.id.btnSubmit, R.id.img1, R.id.img2, R.id.img3, R.id.img4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img1:
                pickImage(1);
                break;
            case R.id.img2:
                pickImage(2);
                break;
            case R.id.img3:
                pickImage(3);
                break;
            case R.id.img4:
                pickImage(4);
                break;
            case R.id.btnUpload1:
                pickImage(1);
                break;
            case R.id.btnUpload2:
                pickImage(2);
                break;
            case R.id.btnUpload3:
                pickImage(3);
                break;
            case R.id.btnUpload4:
                pickImage(4);
                break;
            case R.id.btnSubmit:
                String name = edtName.getText().toString().trim();
                String email = edtEmail.getText().toString().trim();
                String notes = edtNotes.getText().toString().trim();

                if (TextUtils.isEmpty(name)) {
                    edtName.requestFocus();
                    edtName.setError("Enter Name");
                } else if (TextUtils.isEmpty(email)) {
                    edtEmail.requestFocus();
                    edtEmail.setError("Enter Email");
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    edtEmail.requestFocus();
                    edtEmail.setError("Enter Valid Email");
                } else if (TextUtils.isEmpty(notes)) {
                    edtNotes.setError("Write something..");
                    edtNotes.requestFocus();
                } else if (surveyImagesParts == null) {
                    Toast.makeText(this, "Please select images", Toast.LENGTH_SHORT).show();
                } else {
                    if (Network.isConnected(this)) {
                        uploadImages(notes,name,email);
                    } else {
                        Toast.makeText(this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }
    }

    private void uploadImages(String notes, String name, String email) {
        mProgressDialog.show();
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody draBody = RequestBody.create(MediaType.parse("text/plain"), notes);
        surveyImagesParts = new MultipartBody.Part[imagePaths.size()];

        for (int index = 0; index < imagePaths.size(); index++) {

            File file = new File(imagePaths.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            surveyImagesParts[index] = MultipartBody.Part.createFormData("file[]", file.getName(), surveyBody);
        }
        mApiInterfaceListener.doSpecialImages(surveyImagesParts, draBody,nameBody,emailBody).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    SpecialImageResponse specialImageResponse = new Gson().fromJson(response.body(), SpecialImageResponse.class);
                    switch (specialImageResponse.getStatus()) {
                        case "200":
                            clearFeilds();
                            MessageUtil.showToast(SpecialImagesActivity.this, specialImageResponse.getMessage());
                            break;
                        default:
                            MessageUtil.showToast(SpecialImagesActivity.this, specialImageResponse.getMessage());
                            break;
                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                t.printStackTrace();
                mProgressDialog.dismiss();
            }
        });
    }

    private void clearFeilds() {
        edtName.setText("");
        edtEmail.setText("");
        edtNotes.setText("");
        img1.setImageResource(R.drawable.ic_upload_image);
        img2.setImageResource(R.drawable.ic_upload_image);
        img3.setImageResource(R.drawable.ic_upload_image);
        img4.setImageResource(R.drawable.ic_upload_image);
        surveyImagesParts = null;
    }

    private void pickImage(int pos) {
        if (PermissionUtil.checkPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            this.pos = pos;
            new ImagePicker.Builder(SpecialImagesActivity.this)
                    .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                    .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                    .directory(ImagePicker.Directory.DEFAULT)
                    .extension(ImagePicker.Extension.PNG)
                    .allowMultipleImages(false)
                    .enableDebuggingMode(false)
                    .build();
        } else {
            PermissionUtil.requestPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE, 120);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImagePicker.IMAGE_PICKER_REQUEST_CODE:
                switch (resultCode) {
                    case RESULT_OK:
                        List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
                        if (mPaths.size() > 0) {
                            showImage(mPaths.get(0));
                        }

                        break;
                }
                break;
        }

    }

    private void showImage(String path) {
        switch (pos) {
            case 1:
                Glide.with(SpecialImagesActivity.this).asBitmap().apply(new RequestOptions().error(R.drawable.ic_upload_image).placeholder(R.drawable.ic_upload_image)).load(path).into(img1);
                addToPost(path, 0);
                break;
            case 2:
                Glide.with(SpecialImagesActivity.this).asBitmap().apply(new RequestOptions().error(R.drawable.ic_upload_image).placeholder(R.drawable.ic_upload_image)).load(path).into(img2);
                addToPost(path, 1);
                break;
            case 3:
                Glide.with(SpecialImagesActivity.this).asBitmap().apply(new RequestOptions().error(R.drawable.ic_upload_image).placeholder(R.drawable.ic_upload_image)).load(path).into(img3);
                addToPost(path, 2);
                break;
            case 4:
                Glide.with(SpecialImagesActivity.this).asBitmap().apply(new RequestOptions().error(R.drawable.ic_upload_image).placeholder(R.drawable.ic_upload_image)).load(path).into(img4);
                addToPost(path, 3);
                break;
        }
    }

    private void addToPost(String path, int index) {
        if (imagePaths != null) {
            imagePaths.add(path);
        }
        /*File propertyImageFile = new File(path);
        RequestBody propertyImage = RequestBody.create(MediaType.parse("image/*"), propertyImageFile);
        MultipartBody.Part propertyImagePart = MultipartBody.Part.createFormData("files[]", propertyImageFile.getName(), propertyImage);
        if (surveyImagesParts != null) {
            surveyImagesParts.add(propertyImagePart);
        }*/
    }
}
