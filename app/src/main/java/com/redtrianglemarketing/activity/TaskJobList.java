package com.redtrianglemarketing.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.TaskListAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.SubModel.JobtaskList_;
import com.redtrianglemarketing.Model.request.JobtaskList;
import com.redtrianglemarketing.Model.request.TaskFolderListRequest;
import com.redtrianglemarketing.Model.response.TaskFolderListResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskJobList extends AppCompatActivity {

    RecyclerView recyclerView;
    Context context;
    ApiInterfaceListener listener;
    TaskListAdapter adapter;
    Toolbar toolbar;
    ProgressDialog mProgressDialog;
    String job_id="",folder_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_job_list);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.folder_list_recycler);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        initDialog();
        context = getApplicationContext();
        AppConstants.CONTEXT=context;
        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        if (getIntent().getExtras()!=null)
        {
            job_id=getIntent().getStringExtra(AppConstants.RequestDataKey.job_id);
            folder_id=getIntent().getStringExtra(AppConstants.RequestDataKey.folder_id);
            String title=getIntent().getStringExtra("title");
            getSupportActionBar().setTitle(title);
            if (Network.isConnected(TaskJobList.this)) {
                taskList();
            } else {
                Toast.makeText(TaskJobList.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
            }

        }

    }
    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }
    private void taskList()
    {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<TaskFolderListResponse> call=listener.taskList(user_id,api_token,job_id,folder_id);
        call.enqueue(new Callback<TaskFolderListResponse>() {
            @Override
            public void onResponse(Call<TaskFolderListResponse> call, Response<TaskFolderListResponse> response)
            {
                mProgressDialog.dismiss();
                if (response.isSuccessful() && response.body() != null)
                {
                    TaskFolderListResponse taskResponse = response.body();
                    Log.e("taskResponse", taskResponse.toString());
                    if (taskResponse != null) {
                        TaskFolderListRequest request = taskResponse.getData();
                        List<JobtaskList> jobLists = request.getJobtaskList();

                        for (int i = 0; i < jobLists.size(); i++) {
                            JobtaskList jobtaskList = jobLists.get(i);
                            List<JobtaskList_> list_ = jobtaskList.getJobtaskList();
                            setAdapter(list_);
                        }
                    } else {
                        mProgressDialog.dismiss();
                    }
                } else {
                    try {
                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<TaskFolderListResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Log.e("taskResponse",t.toString());
            }
        });
    }

    private void setAdapter(List<JobtaskList_> list) {
        if (list.size() > 0) {
            adapter = new TaskListAdapter(this, list);
            recyclerView.setAdapter(adapter);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
