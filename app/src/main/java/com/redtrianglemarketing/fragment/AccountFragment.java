package com.redtrianglemarketing.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.response.ProfileUpdateResponse;
import com.redtrianglemarketing.Model.response.UserAuthenticated;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import retrofit2.Callback;
import retrofit2.Response;

public class AccountFragment extends Fragment implements View.OnClickListener {
    View view;
    EditText name,mail;
    Button submit;
    ApiInterfaceListener listener;
    Context context;
    ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.account_fragment,container,false);
        name=view.findViewById(R.id.acc_name);
        mail=view.findViewById(R.id.acc_mail);
        submit=view.findViewById(R.id.acc_submit);

        context=getContext();
        AppConstants.CONTEXT=context;
        initDialog();
        listener= APIClient.getClient().create(ApiInterfaceListener.class);

        String email= Pref.getValue(AppConstants.preDataKey.email,"");
        String userName= Pref.getValue(AppConstants.preDataKey.name,"");
        mail.setText(email);
        name.setText(userName);

        submit.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view)
    {
      switch (view.getId())
      {
         case R.id.acc_submit:
             if (Network.isConnected(getActivity())) {
                 updateProfile();
             } else {
                 Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
             }

          break;
      }
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void updateProfile()
    {
        mProgressDialog.show();
        if (name.getText().toString().trim()!=null)
        {
            String api_token= Pref.getValue(AppConstants.preDataKey.api_token,"");
            String user_id=Pref.getValue(AppConstants.preDataKey.user_id,"");
            String uname=name.getText().toString();

            retrofit2.Call<ProfileUpdateResponse> call=listener.updateProfile(api_token,user_id,uname);
            call.enqueue(new Callback<ProfileUpdateResponse>() {
                @Override
                public void onResponse(retrofit2.Call<ProfileUpdateResponse> call, Response<ProfileUpdateResponse> response)
                {
                    mProgressDialog.dismiss();
                    if (response.body().getStatus().equals("true"))
                    {
                        UserAuthenticated authenticated=response.body().getResponse();
                        Pref.setValue(AppConstants.preDataKey.name,authenticated.name);
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        mProgressDialog.dismiss();
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(retrofit2.Call<ProfileUpdateResponse> call, Throwable t) {
                    Toast.makeText(getContext(),"Profile Not Update", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });

        }
    }

}
