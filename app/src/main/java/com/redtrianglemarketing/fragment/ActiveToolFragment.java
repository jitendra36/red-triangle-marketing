package com.redtrianglemarketing.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.redtrianglemarketing.Adapter.ActiveToolAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.ToolListActiveRequest;
import com.redtrianglemarketing.Model.response.ToolListActiveResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActiveToolFragment extends Fragment {


    @BindView(R.id.recyclerTools)
    RecyclerView recyclerTools;
    Unbinder unbinder;

    List<ToolListActiveRequest> data;
    ApiInterfaceListener listener;
    ActiveToolAdapter toolAdapter;
    @BindView(R.id.txtNoData)
    TextView txtNoData;
    private ProgressDialog  mProgressDialog;

    public ActiveToolFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tools, container, false);
        unbinder = ButterKnife.bind(this, view);

        initDialog();
        data = new ArrayList<ToolListActiveRequest>();


        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        if (Network.isConnected(getActivity())) {
            toolsUsedList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }

        return view;
    }


    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void initList(List<ToolListActiveRequest> data) {
        recyclerTools.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        toolAdapter = new ActiveToolAdapter(getActivity(), data);
        toolAdapter.setFragment(this);
        recyclerTools.setAdapter(toolAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void toolsUsedList() {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<ToolListActiveResponse> call = listener.toolsUsedList(user_id, api_token);
        call.enqueue(new Callback<ToolListActiveResponse>() {
            @Override
            public void onResponse(Call<ToolListActiveResponse> call, Response<ToolListActiveResponse> response) {
                mProgressDialog.dismiss();
                ToolListActiveResponse request = response.body();
                if (request != null) {
                    data = request.getData();
                    if (data.size()>0){
                        initList(data);
                    }else {
                        recyclerTools.setVisibility(View.GONE);
                        txtNoData.setVisibility(View.VISIBLE);
                    }

                } else {
                    recyclerTools.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                    mProgressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<ToolListActiveResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });

    }

    public void UpdateList() {
        toolAdapter.clear();
        toolsUsedList();
    }
}
