package com.redtrianglemarketing.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.response.ChangePasswordResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends Fragment implements View.OnClickListener {
    View view;
    EditText mail, oldpass, newpass, confpass;
    Button update;
    ApiInterfaceListener listener;
    Context context;
    ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.change_password_fragment, container, false);

        mail = view.findViewById(R.id.email_user);
        oldpass = view.findViewById(R.id.old_pass);
        newpass = view.findViewById(R.id.new_pass);
        confpass = view.findViewById(R.id.conf_pass);
        update = view.findViewById(R.id.update_btn);
        context = getContext();
        AppConstants.CONTEXT = context;
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        update.setOnClickListener(this);

        String emailUser = Pref.getValue(AppConstants.preDataKey.email, "");
        mail.setText(emailUser);
        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.update_btn:
                if (Network.isConnected(getActivity())) {
                    updateUser();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }
    //user password update  method
    private void updateUser() {
        if (validation() == true) {
            String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
            String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
            final String old_password = oldpass.getText().toString();
            String new_password = newpass.getText().toString();
            String con_password = confpass.getText().toString();

            retrofit2.Call<ChangePasswordResponse> call = listener.changePassword(api_token, new_password, con_password, user_id, old_password);
            call.enqueue(new Callback<ChangePasswordResponse>()
            {
                @Override
                public void onResponse(retrofit2.Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                    Log.e("getMessage",response.body().getMessage());
                    Log.e("getCode",response.body().getCode());
                    Log.e("getStatus",response.body().getStatus());

                    if (response.body()!=null && response.body().getStatus().equals("true"))
                    {
                        //clear text after password change
                        oldpass.getText().clear();
                        newpass.getText().clear();
                        confpass.getText().clear();

                        //change fragment after password change
                        Fragment fragment = new HomeFragment();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.flContent, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else
                        {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                }

                @Override
                public void onFailure(retrofit2.Call<ChangePasswordResponse> call, Throwable t)
                {
                    Toast.makeText(getContext(),call.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }


    }

    //change password validation after password change method call all requirement are fulfill
    private boolean validation() {
        boolean value = true;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

      if   (!(oldpass.getText().toString().trim().length() >= 6))
        {

            oldpass.requestFocus();
            oldpass.setError("minimum six digit required");
            Toast.makeText(getActivity(), "Input password fail", Toast.LENGTH_LONG).show();

            value = false;
        }
        if (!(newpass.getText().toString().trim().length() >= 6))
        {

            newpass.requestFocus();
            newpass.setError("Minimum six digit required");

            value = false;
        }
        else if (!(newpass.getText().toString().trim().equals(confpass.getText().toString().trim())))
        {

            confpass.requestFocus();
            confpass.setError("Password does not match");

            value = false;
        }
        if ((confpass.getText().toString().trim().equals(oldpass.getText().toString().trim())))
        {
            Toast.makeText(getContext(),"old password and new password can not be same", Toast.LENGTH_SHORT).show();
            value = false;
        }
        else
        {
            value = true;
        }
        return value;
    }

}
