package com.redtrianglemarketing.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.DocumentListAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.DocumentListModel;
import com.redtrianglemarketing.Model.request.DocumentRequest;
import com.redtrianglemarketing.Model.response.DocumentResponse;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.FileUtils;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.PermissionUtil;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.Utility.ProgressRequestBody;
import com.redtrianglemarketing.activity.FolderListActivity;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentList extends Fragment implements ProgressRequestBody.UploadCallbacks {
    View view;
    RecyclerView recyclerView;
    ApiInterfaceListener listener;
    Context context;
    String jobId, folderId;
    Dialog dialog;
    Uri path;

    FloatingActionButton actionButton;
    String user_id;
    String api_token;
    String d_title;
    String d_disc;
    private ProgressDialog mProgressDialog, mShowProcess;
    private GridLayoutManager lLayoutPost;
    DocumentListAdapter documentListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.folder_fragmemt, container, false);
        recyclerView = view.findViewById(R.id.folder_list);
        lLayoutPost = new GridLayoutManager(getActivity(), 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(lLayoutPost);
        actionButton = view.findViewById(R.id.vid_flot);

        initProgress();
        initDialog();
        context = getContext();
        AppConstants.CONTEXT = context;


        FolderListActivity activity = (FolderListActivity) getActivity();
        jobId = activity.getJobId();
        folderId = activity.getFolderId();

        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!PermissionUtil.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    PermissionUtil.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, 1022);
                } else if (!PermissionUtil.checkPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    PermissionUtil.requestPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE, 1022);
                }
                if (Network.isConnected(getActivity())) {
                    addDocument();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        if (Network.isConnected(getActivity())) {
            listDocument();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    private void initProgress() {
        mShowProcess = new ProgressDialog(getActivity());
        mShowProcess.setMessage("Please wait");
        mShowProcess.setIndeterminate(true);
        mShowProcess.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mShowProcess.setCancelable(false);
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }
    private void listDocument() {
        mProgressDialog.show();
        retrofit2.Call<DocumentResponse> call = listener.documentList(user_id, api_token, jobId, folderId);
        call.enqueue(new Callback<DocumentResponse>() {
            @Override
            public void onResponse(retrofit2.Call<DocumentResponse> call, Response<DocumentResponse> response) {
                mProgressDialog.dismiss();
                DocumentResponse documentResponse = response.body();
                if (documentResponse != null) {
                    DocumentListModel listModel = documentResponse.getData();
                    List<DocumentRequest> list = listModel.getDocumentDetail();
                    setAdapter(list);
                } else {
                }

            }

            @Override
            public void onFailure(retrofit2.Call<DocumentResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

    private void setAdapter(List<DocumentRequest> list) {
        if (list.size() > 0) {
            documentListAdapter = new DocumentListAdapter(context,list);
            recyclerView.setAdapter(documentListAdapter);
        }


    }

    public void addDocument() {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_alert_card);
        final TextView text = dialog.findViewById(R.id.textView8);
        final EditText jobTitle = dialog.findViewById(R.id.add_job_title);
        final EditText Title = dialog.findViewById(R.id._title);
        final Button submit = dialog.findViewById(R.id.button);
        Title.setVisibility(View.VISIBLE);
        text.setText(String.format(Locale.getDefault(), "%s", "Add Document"));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!jobTitle.getText().toString().trim().equals("")) {

                    d_title = Title.getText().toString().trim();
                    d_disc = jobTitle.getText().toString().trim();
                    showFileChooser();

                } else {
                    Toast.makeText(context, "Title Is Not Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
//        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    1);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                path = data.getData();
                Log.e("", "File : " + path);
                if (path != null) {
                    addNewDocument();
                    dialog.dismiss();
                }

                dialog.dismiss();

            }
        }
    }


    private void addNewDocument() {
        dialog.dismiss();
        mShowProcess.show();
        File file = FileUtils.getFile(getActivity(), path);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
        retrofit2.Call<SimpleResponse> call = listener.addDocument(user_id, api_token, jobId, folderId, d_title, d_disc, imageFileBody);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<SimpleResponse> call, @NonNull Response<SimpleResponse> response) {
                mShowProcess.dismiss();
                if (response.isSuccessful()) {
                    Log.e("SimpleResponse succ", response.body().toString());
                    SimpleResponse simpleResponse = response.body();
                    listDocument();
                    Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        Log.e("SimpleResponse error", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<SimpleResponse> call, Throwable t) {
                mShowProcess.dismiss();
                Log.e("SimpleResponse error2", call.toString());
            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
