package com.redtrianglemarketing.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.FolderListAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.FolderListRequest;
import com.redtrianglemarketing.Model.request.JobfolderList;
import com.redtrianglemarketing.Model.response.FolderListResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.activity.HomeActivity;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Folder extends Fragment {

    View view;
    RecyclerView recyclerView;
    ApiInterfaceListener listener;
    Context context;
    String job_id;
    ProgressDialog mProgressDialog;
    FolderListAdapter mFolderListAdapter;
    private GridLayoutManager lLayoutPost;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.folder_fragmemt, container, false);
        recyclerView = view.findViewById(R.id.folder_list);
        lLayoutPost = new GridLayoutManager(getActivity(), 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(lLayoutPost);
        FloatingActionButton button = view.findViewById(R.id.vid_flot);
        button.setVisibility(View.GONE);

        context = getContext();
        AppConstants.CONTEXT = context;
        initDialog();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        HomeActivity activity = (HomeActivity) getActivity();
        job_id = activity.getId();

        if (Network.isConnected(getActivity())) {
            listFolder();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }

        return view;
    }
    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void listFolder() {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<FolderListResponse> call = listener.folderList(user_id, api_token, job_id);
        call.enqueue(new Callback<FolderListResponse>() {
            @Override
            public void onResponse(Call<FolderListResponse> call, Response<FolderListResponse> response) {
               mProgressDialog.dismiss();
                if (response.isSuccessful() && response.body() != null)
                {
                    FolderListResponse listResponse = response.body();
                    if (listResponse != null) {
                        List<FolderListRequest> folderList = listResponse.getData();
                        Log.e("folderList", folderList.toString());

                        for (int i = 0; i < folderList.size(); i++) {
                            FolderListRequest listRequest = folderList.get(i);
                            List<JobfolderList> list = listRequest.getJobfolderList();
                            setAdapter(list);
                        }
                    } else {
                        mProgressDialog.dismiss();
                    }
                } else {
                    try {
                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<FolderListResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

    private void setAdapter(List<JobfolderList> list) {
        if (list.size() > 0) {
            mFolderListAdapter = new FolderListAdapter(getActivity(),list);
            recyclerView.setAdapter(mFolderListAdapter);
        }
    }
}
