package com.redtrianglemarketing.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.ChangeOrderListAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.JobCard;
import com.redtrianglemarketing.Model.request.JobCardRequest;
import com.redtrianglemarketing.Model.response.JobCardResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.activity.CreateJobCardActivity;
import com.redtrianglemarketing.activity.HomeActivity;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobCardFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    ApiInterfaceListener listener;
    Context context;
    String job_id;
    ChangeOrderListAdapter adapter;
    ProgressDialog mProgressDialog;
    private BroadcastReceiver mBroadcastReceiver;
    FloatingActionButton actionButton;
    private GridLayoutManager lLayoutPost;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.jobcard_fragment, container, false);

        recyclerView=view.findViewById(R.id.jobcard_list);
        lLayoutPost = new GridLayoutManager(getActivity(), 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(lLayoutPost);
        actionButton=view.findViewById(R.id.fab);

        context=getContext();
        AppConstants.CONTEXT=context;
        initDialog();
        listener= APIClient.getClient().create(ApiInterfaceListener.class);

        HomeActivity activity = (HomeActivity) getActivity();
        job_id= activity.getId();
        if (Network.isConnected(getActivity())) {
            jobCardList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }


        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Network.isConnected(context)) {
                    jobCardList();
                } else {
                    Toast.makeText(context, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver, new IntentFilter("com.mjmmechanical.ACTION_UPDATE"));



        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Network.isConnected(getActivity())) {
                    createJobCard();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        return view;
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }
    private void jobCardList()
    {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<JobCardResponse> call=listener.jobCardList(user_id,api_token,job_id);
        call.enqueue(new Callback<JobCardResponse>() {
            @Override
            public void onResponse(Call<JobCardResponse> call, Response<JobCardResponse> response)
            {
                mProgressDialog.dismiss();
                if (response.isSuccessful() && response.body() != null)
                {
                    JobCardResponse cardResponse = response.body();
                    if (cardResponse != null) {
                        JobCardRequest request = cardResponse.getData();
                        List<JobCard> cards = request.getJobCard();
                        Log.e("jobcard", cards.toString());
                        setAdapter(cards);
                    } else {
                        mProgressDialog.dismiss();
                    }
                } else {
                    try {
                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<JobCardResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

    private void setAdapter(List<JobCard> list) {
        if (list.size() > 0) {
             adapter = new ChangeOrderListAdapter(getActivity(),list);
            recyclerView.setAdapter(adapter);

        }
    }

    public void createJobCard()
    {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_alert_card);
        final EditText jobTitle = dialog.findViewById(R.id.add_job_title);
        final Button submit = dialog.findViewById(R.id.button);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (!jobTitle.getText().toString().trim().equals(""))
                {
                    dialog.dismiss();
                    Intent intent=new Intent(getActivity().getBaseContext(),CreateJobCardActivity.class);
                    intent.putExtra(AppConstants.RequestDataKey.job_title,jobTitle.getText().toString());
                    intent.putExtra(AppConstants.RequestDataKey.job_id,job_id);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(context,"Title Is Not Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
    }
}
