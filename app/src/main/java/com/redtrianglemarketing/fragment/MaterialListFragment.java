package com.redtrianglemarketing.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.MaterialListAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.SubModel.MaterialList;
import com.redtrianglemarketing.Model.request.MaterialListRequest;
import com.redtrianglemarketing.Model.response.MaterialListResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;
import com.redtrianglemarketing.paginate.Paginate;
import com.redtrianglemarketing.recycler.LoadingListItemCreator;
import com.redtrianglemarketing.recycler.LoadingListItemSpanLookup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class MaterialListFragment extends Fragment implements Paginate.Callbacks  {


    ApiInterfaceListener listener;

    MaterialListAdapter adapter;
    int totalPageFromApi;
    List<MaterialList> data;
    private boolean loading = false;
    private int page;
    protected long networkDelay = 2000;
    protected int totalPages;
    private Handler handler;
    protected int threshold = 4;
    private Paginate paginate;
    protected boolean customLoadingListItem = false;
    private static final int GRID_SPAN = 3;
    protected boolean addLoadingRow = true;
    LinearLayoutManager llm;

    private TextView textView;
    private RecyclerView recyclerView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_list, container, false);
        textView = view.findViewById(R.id.txtNoData);
        recyclerView = view.findViewById(R.id.list);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(llm);
        handler = new Handler();
        data = new ArrayList<>();

        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        if (Network.isConnected(getActivity())) {
            loadList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }


        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void loadList() {

        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        retrofit2.Call<MaterialListResponse> call = listener.listMaterial(user_id, api_token, "1");
        call.enqueue(new Callback<MaterialListResponse>() {
            @Override
            public void onResponse(retrofit2.Call<MaterialListResponse> call, Response<MaterialListResponse> response) {

                MaterialListResponse request = response.body();
                if (request != null) {
                    MaterialListRequest listData = request.getData();
                    totalPageFromApi = listData.getLastPage();
                    page = 1;
                    hasLoadedAllItems();
                    data = listData.getData();
                    Log.e("JobListRequest", data.toString());
                    if (data.size() > 0) {
                        setupPagination(data);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        textView.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onFailure(retrofit2.Call<MaterialListResponse> call, Throwable t) {

            }
        });
    }

    private void setpaginateData(int page) {
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        retrofit2.Call<MaterialListResponse> call = listener.listMaterial(user_id, api_token, String.valueOf(page));
        call.enqueue(new Callback<MaterialListResponse>() {
            @Override
            public void onResponse(retrofit2.Call<MaterialListResponse> call, Response<MaterialListResponse> response) {
                MaterialListResponse request = response.body();
                if (request != null) {
                    MaterialListRequest listData = request.getData();
                    data = listData.getData();

                    if (data.size() > 0) {
                        adapter.add(data);
                        loading = false;
                    } else {
                        loading = true;
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<MaterialListResponse> call, Throwable t) {
            }
        });
    }


    protected void setupPagination(List<MaterialList> list) {
        if (paginate != null) {
            paginate.unbind();
        }
        handler.removeCallbacks(fakeCallback);
        adapter = new MaterialListAdapter(list);
        loading = false;

        recyclerView.setAdapter(adapter);
        Log.e("totalPageFromApi", String.valueOf(totalPageFromApi));
        if (totalPageFromApi > 1) {

            paginate = Paginate.with(recyclerView, this)
                    .setLoadingTriggerThreshold(threshold)
                    .addLoadingListItem(addLoadingRow)
                    .setLoadingListItemCreator(customLoadingListItem ? new CustomLoadingListItemCreator() : null)
                    .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                        @Override
                        public int getSpanSize() {
                            return GRID_SPAN;
                        }
                    })
                    .build();
        }
    }

    @Override
    public synchronized void onLoadMore() {
        Log.d("Paginate", "onLoadMore");
        loading = true;
        handler.postDelayed(fakeCallback, networkDelay);
    }

    @Override
    public synchronized boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        Log.e("page", String.valueOf(page));
        totalPages = totalPageFromApi;
        return page == totalPages;
    }

    private Runnable fakeCallback = new Runnable() {
        @Override
        public void run() {
            {
                page++;

                if (page < totalPages + 1) {
                    Log.e("page", String.valueOf(page));
                    if (Network.isConnected(getActivity())) {
                        setpaginateData(page);
                    } else {
                        Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    loading = true;
                }
            }
        }
    };


    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.custom_loading_list_item, parent, false);
            return new ActiveJobFragment.VH(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            VH vh = (VH) holder;
            vh.tvLoading.setText(String.format("Total items loaded: %d.\nLoading more...", adapter.getItemCount()));
            if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
                StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) vh.itemView.getLayoutParams();
                params.setFullSpan(true);
            }
        }
    }

    static class VH extends RecyclerView.ViewHolder {
        TextView tvLoading;

        public VH(View itemView) {
            super(itemView);
            tvLoading = itemView.findViewById(R.id.tv_loading_text);
        }
    }

}
