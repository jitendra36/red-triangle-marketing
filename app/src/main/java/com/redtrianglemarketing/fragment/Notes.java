package com.redtrianglemarketing.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.NotesRecyclerAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.NotesRequest;
import com.redtrianglemarketing.Model.response.NotesResponse;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.activity.HomeActivity;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static net.alhazmy13.mediapicker.Image.ImageTags.Tags.TAG;

public class Notes extends Fragment {
    private View view;
    private RecyclerView recyclerView;
    private ApiInterfaceListener listener;
    private Context context;
    private String job_id;
    private LinearLayoutManager llm;
    private FloatingActionButton actionButton;
    Dialog dialog;
    ProgressDialog mProgressDialog;
    public List<NotesRequest> request;
    private NotesRecyclerAdapter adapter;

    public Notes() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.notes_fragment, container, false);

        recyclerView = view.findViewById(R.id.notes_list);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(llm);

        actionButton = view.findViewById(R.id.fab);

        context = getContext();
        AppConstants.CONTEXT = context;
        initDialog();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        HomeActivity activity = (HomeActivity) getActivity();
        job_id = activity.getId();

        if (Network.isConnected(getActivity())) {
            notesList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Network.isConnected(getActivity())) {
                    createNotes();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    private void createNotes()
    {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_alert_card);
        final EditText editTitle=dialog.findViewById(R.id._title);
        editTitle.setVisibility(View.GONE);
        final EditText jobTitle = dialog.findViewById(R.id.add_job_title);
        final Button submit = dialog.findViewById(R.id.button);
        final TextView title = dialog.findViewById(R.id.textView8);


        title.setText(String.format(Locale.getDefault(), "%s", "Add Notes"));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(jobTitle.getText().toString())) {
                    String notes = jobTitle.getText().toString().trim();
                    addNotes(notes);
                } else {
                    Toast.makeText(context, "Notes Is Not Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    public void notesList()
    {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<NotesResponse> call = listener.notesList(user_id, api_token, job_id);
        call.enqueue(new Callback<NotesResponse>() {
            @Override
            public void onResponse(Call<NotesResponse> call, Response<NotesResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()){
                    NotesResponse notesResponse = response.body();
                    request = notesResponse.getData();
                    setAdapter(request);
                }else {
                    try {
                        Log.e(TAG, "onResponse: "+response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }



            }

            @Override
            public void onFailure(Call<NotesResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Log.e("NotesRequest", t.toString());
            }
        });


    }

    private void addNotes(String notes)
    {
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<SimpleResponse> call = listener.notesCreate(user_id, api_token, job_id, notes);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    SimpleResponse simpleResponse = response.body();
                    notesList();
                    Toast.makeText(getActivity(), simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }
    private void setAdapter(List<NotesRequest> list) {
        if (list.size() > 0) {
            adapter = new NotesRecyclerAdapter(getActivity(), list,Notes.this);
            recyclerView.setAdapter(adapter);
        }
    }
}
