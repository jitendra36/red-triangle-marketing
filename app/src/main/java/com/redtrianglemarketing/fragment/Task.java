package com.redtrianglemarketing.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.TaskFolderAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.JobfolderList;
import com.redtrianglemarketing.Model.request.TaskRequest;
import com.redtrianglemarketing.Model.response.TaskResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.activity.HomeActivity;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static net.alhazmy13.mediapicker.Image.ImageTags.Tags.TAG;

public class Task extends Fragment {
    View view;
    Context context;
    ApiInterfaceListener listener;
    RecyclerView recyclerView;
    TaskFolderAdapter folderAdapter;
    String job_id;
    ProgressDialog mProgressDialog;
    private GridLayoutManager lLayoutPost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.task_fragment, container, false);

        recyclerView = view.findViewById(R.id.list);
        lLayoutPost = new GridLayoutManager(getActivity(), 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(lLayoutPost);

        context = getContext();
        AppConstants.CONTEXT = context;
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        initDialog();
        HomeActivity activity = (HomeActivity) getActivity();
        job_id = activity.getId();

        if (Network.isConnected(getActivity())) {
            taskFolderList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void taskFolderList()
    {
    mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<TaskResponse> call = listener.taskFolderList(user_id, api_token, job_id);
        call.enqueue(new Callback<TaskResponse>() {
            @Override
            public void onResponse(Call<TaskResponse> call, Response<TaskResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    TaskResponse taskResponse = response.body();
                    if (taskResponse != null) {
                        List<TaskRequest> requests = taskResponse.getData();
                        for (int i = 0; i < requests.size(); i++) {
                            TaskRequest request = requests.get(i);
                            List<JobfolderList> lists = request.getJobfolderList();
                            setFolderAdapter(lists);
                        }
                    } else {
                        mProgressDialog.dismiss();
                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<TaskResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }
    private void setFolderAdapter(List<JobfolderList> list) {
        if (list.size() > 0) {
            folderAdapter = new TaskFolderAdapter(view.getContext(), list);
            recyclerView.setAdapter(folderAdapter);
        }
    }
}
