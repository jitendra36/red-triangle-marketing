package com.redtrianglemarketing.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.redtrianglemarketing.Adapter.VideoListAdapter;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.request.VideoListModel;
import com.redtrianglemarketing.Model.request.VideoRequest;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.Model.response.VideoResponse;
import com.redtrianglemarketing.R;
import com.redtrianglemarketing.Utility.Network;
import com.redtrianglemarketing.Utility.PermissionUtil;
import com.redtrianglemarketing.Utility.Pref;
import com.redtrianglemarketing.Utility.ProgressRequestBody;
import com.redtrianglemarketing.activity.FolderListActivity;
import com.redtrianglemarketing.network.APIClient;
import com.redtrianglemarketing.network.ApiInterfaceListener;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class VideoList extends Fragment implements ProgressRequestBody.UploadCallbacks {
    private static final String TAG = VideoList.class.getSimpleName();
    View view;
    RecyclerView recyclerView;
    ApiInterfaceListener listener;
    Context context;
    String jobId, folderId;
    FloatingActionButton actionButton;
    String user_id;
    ProgressDialog mProgressDialog;
    String api_token;
    File file = null;
    Dialog dialog;
    String d_title;
    String d_disc;
    private GridLayoutManager lLayoutPost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.folder_fragmemt, container, false);
        recyclerView = view.findViewById(R.id.folder_list);
        lLayoutPost = new GridLayoutManager(getActivity(), 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(lLayoutPost);
        actionButton = view.findViewById(R.id.vid_flot);

        context = getContext();
        AppConstants.CONTEXT = context;
        initDialog();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        FolderListActivity activity = (FolderListActivity) getActivity();
        jobId = activity.getJobId();
        folderId = activity.getFolderId();
        user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!PermissionUtil.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    PermissionUtil.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, 1022);
                } else if (!PermissionUtil.checkPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    PermissionUtil.requestPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE, 1022);
                } else {
                    if (Network.isConnected(getActivity())) {
                        addVideo();
                    } else {
                        Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        if (Network.isConnected(getActivity())) {
            listVideo();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    private void listVideo() {
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<VideoResponse> call = listener.videoList(user_id, api_token, jobId, folderId);
        call.enqueue(new Callback<VideoResponse>() {
            @Override
            public void onResponse(Call<VideoResponse> call, Response<VideoResponse> response) {
                if (response.isSuccessful()) {
                    VideoResponse videoResponse = response.body();
                    if (videoResponse != null) {
                        VideoListModel listModel = videoResponse.getData();
                        List<VideoRequest> list = listModel.getVideoDetail();
                        setAdapter(list);
                    } else {
                        Log.e("onResponse: ", "videoResponse is null");
                    }
                } else {
                    try {
                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<VideoResponse> call, Throwable t) {
            }
        });
    }

    private void setAdapter(List<VideoRequest> list) {
        if (list.size() > 0) {
            VideoListAdapter adapter = new VideoListAdapter(getActivity(), list);
            recyclerView.setAdapter(adapter);
        }
    }

    public void addVideo() {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_alert_card);
        final TextView text = dialog.findViewById(R.id.textView8);
        final EditText jobTitle = dialog.findViewById(R.id.add_job_title);
        final EditText Title = dialog.findViewById(R.id._title);
        final Button submit = dialog.findViewById(R.id.button);
        Title.setVisibility(View.VISIBLE);
        text.setText(String.format(Locale.getDefault(), "%s", "Add Video"));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!jobTitle.getText().toString().trim().equals("")) {

                    d_title = Title.getText().toString().trim();
                    d_disc = jobTitle.getText().toString().trim();
                    pickVideo();

                } else {
                    Toast.makeText(context, "Title Is Not Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
    }

    private void pickVideo() {
        Intent pickVideo = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickVideo, 3);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {
            Uri selectedImage = intent.getData();
            setFileMethodVideo(selectedImage);
            dialog.dismiss();

        }
    }

    private void setFileMethodVideo(Uri selectedImage) {

        String[] filePathColumn = {MediaStore.Video.Media.DATA};
        android.database.Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        if (cursor == null)
            return;

        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();
        file = new File(filePath);
        long length = file.length();
        length = length / 1024;
        if (!(length > 10240)) {
            addNewVideo();
        } else {
            Toast.makeText(getActivity(), "Video Size larger then 10Mb", Toast.LENGTH_LONG).show();
        }

    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void addNewVideo() {
        mProgressDialog.show();
        String statusMedia = "";
        statusMedia = "video";
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this, statusMedia);
        MultipartBody.Part body = MultipartBody.Part.createFormData("video", file.getName(), fileBody);
        Call<SimpleResponse> call = listener.addVideo(user_id, api_token, jobId, folderId, d_title, d_disc, body);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    SimpleResponse simpleResponse = response.body();
                    Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    listVideo();
                } else {
                    mProgressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Log.d(TAG, "onActivityResult: " + t);
            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}

