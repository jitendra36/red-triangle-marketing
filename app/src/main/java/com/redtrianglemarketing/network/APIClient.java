package com.redtrianglemarketing.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.redtrianglemarketing.Model.LoginDeserializer;
import com.redtrianglemarketing.Model.ProfilerDeserializer;
import com.redtrianglemarketing.Model.response.LoginResponse;
import com.redtrianglemarketing.Model.response.ProfileUpdateResponse;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;
//    private static final String URL="http://18.191.53.95/dev/laravel/ngn-construction/public/api/v1.0/";
    private static final String URL="http://13.127.235.254/dev/laravel/redtrianglemarketing/public/api/v1.0/";

    public static Retrofit getClient()
    {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();


        retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(buildGsonConverter())
                .client(client)
                .build();

        return retrofit;
    }

    private static GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        // Adding custom deserializers
        gsonBuilder.registerTypeAdapter(LoginResponse.class, new LoginDeserializer());
        gsonBuilder.registerTypeAdapter(ProfileUpdateResponse.class,new ProfilerDeserializer());
        Gson myGson = gsonBuilder.create();

        return GsonConverterFactory.create(myGson);
    }
}
