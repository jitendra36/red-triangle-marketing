package com.redtrianglemarketing.network;


import com.google.gson.JsonElement;
import com.redtrianglemarketing.AppConstants.AppConstants;
import com.redtrianglemarketing.Model.response.CardDetailResponse;
import com.redtrianglemarketing.Model.response.ChangePasswordResponse;
import com.redtrianglemarketing.Model.response.DocumentResponse;
import com.redtrianglemarketing.Model.response.FolderListResponse;
import com.redtrianglemarketing.Model.response.JobCardResponse;
import com.redtrianglemarketing.Model.response.JobDetailResponse;
import com.redtrianglemarketing.Model.response.JobListRersponse;
import com.redtrianglemarketing.Model.response.JobMarkResponse;
import com.redtrianglemarketing.Model.response.LoginResponse;
import com.redtrianglemarketing.Model.response.LogoutResponce;
import com.redtrianglemarketing.Model.response.MaterialDetailResponse;
import com.redtrianglemarketing.Model.response.MaterialListResponse;
import com.redtrianglemarketing.Model.response.NotesResponse;
import com.redtrianglemarketing.Model.response.ProductDetailResponse;
import com.redtrianglemarketing.Model.response.ProductResponse;
import com.redtrianglemarketing.Model.response.ProfileUpdateResponse;
import com.redtrianglemarketing.Model.response.RegistrationResponse;
import com.redtrianglemarketing.Model.response.SimpleResponse;
import com.redtrianglemarketing.Model.response.StockUpdateResponce;
import com.redtrianglemarketing.Model.response.TaskFolderListResponse;
import com.redtrianglemarketing.Model.response.TaskResponse;
import com.redtrianglemarketing.Model.response.ToolListActiveResponse;
import com.redtrianglemarketing.Model.response.ToolListInactiveResponse;
import com.redtrianglemarketing.Model.response.VideoResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterfaceListener {
    //login user
    @POST("user/login")
    Call<LoginResponse> loginUser(@Query(AppConstants.RequestDataKey.email) String email,
                                  @Query(AppConstants.RequestDataKey.password) String pwd,
                                  @Query(AppConstants.RequestDataKey.device_token) String device_token,
                                  @Query(AppConstants.RequestDataKey.device_type) String device_type);


    //register user
    @POST("user/registration")
    Call<RegistrationResponse> createUser(@Query(AppConstants.RequestDataKey.name) String name,
                                          @Query(AppConstants.RequestDataKey.email) String email,
                                          @Query(AppConstants.RequestDataKey.password) String pwd,
                                          @Query(AppConstants.RequestDataKey.status) String status,
                                          @Query(AppConstants.RequestDataKey.device_token) String device_token,
                                          @Query(AppConstants.RequestDataKey.device_type) String device_type);

    //Forgot password
    @POST("user/forgot")
    Call<SimpleResponse> forgotPwd(@Query(AppConstants.RequestDataKey.email) String email);

    //Logout
    @POST("user/logout")
    Call<LogoutResponce> logout(@Query(AppConstants.RequestDataKey.api_token) String api_token,
                                @Query(AppConstants.RequestDataKey.user_id) String user_id);

    //Change Password
    @POST("user/changepassword")
    Call<ChangePasswordResponse> changePassword(@Query(AppConstants.RequestDataKey.api_token) String api_token,
                                                @Query(AppConstants.RequestDataKey.password) String password,
                                                @Query(AppConstants.RequestDataKey.password_confirmation) String password_confirmation,
                                                @Query(AppConstants.RequestDataKey.user_id) String user_id,
                                                @Query(AppConstants.RequestDataKey.old_password) String old_password);

    //update Profile
    @POST("user/update")
    Call<ProfileUpdateResponse> updateProfile(@Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                              @Query(AppConstants.RequestDataKey.user_id) String user_id,
                                              @Query(AppConstants.RequestDataKey.name) String name);

    //job listing
    @GET("jobs/archievejoblist")
    Call<JobListRersponse> archiveJobList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                              @Query(AppConstants.RequestDataKey.api_token) String api_token,
                              @Query(AppConstants.RequestDataKey.page) String page,
                              @Query(AppConstants.RequestDataKey.search) String search);
    @GET("jobs/joblist")
    Call<JobListRersponse> jobList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                              @Query(AppConstants.RequestDataKey.api_token) String api_token,
                              @Query(AppConstants.RequestDataKey.page) String page,
                              @Query(AppConstants.RequestDataKey.search) String search);

    //Job list
    @GET("jobs/jobdetail")
    Call<JobDetailResponse> jobDetail(@Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                      @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                      @Query(AppConstants.RequestDataKey.user_id) String user_id);

    //Job mark as done
    @POST("jobs/markasdone")
    Call<JobMarkResponse> markAsDone(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                     @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                     @Query(AppConstants.RequestDataKey.job_id) String job_id);

    //Job task list
    @GET("jobs/jobtaskfolderlist")
    Call<TaskResponse> taskFolderList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                      @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                      @Query(AppConstants.RequestDataKey.job_id) String job_id);

    @GET("jobs/jobtasklist")
    Call<TaskFolderListResponse> taskList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                          @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                          @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                          @Query(AppConstants.RequestDataKey.folder_id) String folder_id);

    //Job folder list
    @GET("jobs/jobfolderlist")
    Call<FolderListResponse> folderList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                        @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                        @Query(AppConstants.RequestDataKey.job_id) String job_id);

    //Job card list
    @GET("jobs/jobcardlist")
    Call<JobCardResponse> jobCardList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                      @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                      @Query(AppConstants.RequestDataKey.job_id) String job_id);


    //Job card list
    @GET("jobs/foldervideodetail")
    Call<VideoResponse> videoList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                  @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                  @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                  @Query(AppConstants.RequestDataKey.folder_id) String folder_id);

    //Job card list
    @GET("jobs/folderdocumentdetail")
    Call<DocumentResponse> documentList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                        @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                        @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                        @Query(AppConstants.RequestDataKey.folder_id) String folder_id);

    //Job card list
    @GET("jobs/jobcarddetail")
    Call<CardDetailResponse> jobCardDetail(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                           @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                           @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                           @Query(AppConstants.RequestDataKey.job_card_id) String job_card_id);

    //Job card list
    @Multipart
    @POST("jobs/createjobcard")
    Call<SimpleResponse> createjobcard(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                       @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                       @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                       @Query(AppConstants.RequestDataKey.data) String data,
                                       @Query(AppConstants.RequestDataKey.job_title) String job_title,
                                       @Part MultipartBody.Part image,
                                       @Query(AppConstants.RequestDataKey.second_client_name) String second_client_name);


    @GET("products/productlist")
    Call<ProductResponse> productList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                      @Query(AppConstants.RequestDataKey.api_token) String api_tokan);

    @GET("products/productdetail")
    Call<ProductDetailResponse> productDetail(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                              @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                              @Query(AppConstants.RequestDataKey.product_id) String product_id);

    @POST("products/productinquiry")
    Call<SimpleResponse> productInquiry(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                        @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                        @Query(AppConstants.RequestDataKey.product_id) String product_id,
                                        @Query(AppConstants.RequestDataKey.name) String name,
                                        @Query(AppConstants.RequestDataKey.emailid) String emailid,
                                        @Query(AppConstants.RequestDataKey.description) String description);

    @POST("jobs/updatejobtask")
    Call<SimpleResponse> jobTaskUpdate(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                       @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                       @Query(AppConstants.RequestDataKey.jobtask_id) String jobtask_id,
                                       @Query(AppConstants.RequestDataKey.jobtask_status) String jobtask_status);


    @GET("jobs/noteslist")
    Call<NotesResponse> notesList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                  @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                  @Query(AppConstants.RequestDataKey.job_id) String job_id);

    @POST("jobs/addnotes")
    Call<SimpleResponse> notesCreate(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                     @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                     @Query(AppConstants.RequestDataKey.job_id) String jobtask_id,
                                     @Query(AppConstants.RequestDataKey.notes) String notes);

    @POST("jobs/deletenotes")
    Call<SimpleResponse> deleteNotes(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                     @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                     @Query(AppConstants.RequestDataKey.note_id) String note_id);

    @POST("jobs/editnotes")
    Call<SimpleResponse> editnotes(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                   @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                   @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                   @Query(AppConstants.RequestDataKey.notes) String notes,
                                   @Query(AppConstants.RequestDataKey.note_id) String note_id);
    @Multipart
    @POST("jobs/adddocument")
    Call<SimpleResponse> addDocument(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                     @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                     @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                     @Query(AppConstants.RequestDataKey.folder_id) String folder_id,
                                     @Query(AppConstants.RequestDataKey.document_title) String document_title,
                                     @Query(AppConstants.RequestDataKey.document_description) String document_description,
                                     @Part MultipartBody.Part image);
    @Multipart
    @POST("jobs/addvideo")
    Call<SimpleResponse> addVideo(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                  @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                  @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                  @Query(AppConstants.RequestDataKey.folder_id) String folder_id,
                                  @Query(AppConstants.RequestDataKey.video_title) String video_title,
                                  @Query(AppConstants.RequestDataKey.video_description) String video_description,
                                  @Part MultipartBody.Part video);


    @GET("listMaterial")
    Call<MaterialListResponse> listMaterial(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                            @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                            @Query(AppConstants.RequestDataKey.page) String page);

    @GET("materialDetail")
    Call<MaterialDetailResponse> materialDetail(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                                @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                                @Query(AppConstants.RequestDataKey.material_id) String material_id);



    @GET("getMaterialUsed")
    Call<StockUpdateResponce> getMaterialUsed(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                              @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                              @Query(AppConstants.RequestDataKey.material_id) String material_id,
                                              @Query(AppConstants.RequestDataKey.quantity) String quantity);

    @GET("checkInOutTools")
    Call<SimpleResponse> checkInOutTools(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                         @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                         @Query(AppConstants.RequestDataKey.tool_id) String tool_id,
                                         @Query(AppConstants.RequestDataKey.in_out_flag) String in_out_flag,
                                         @Query(AppConstants.RequestDataKey.date_time) String date_time);

    @GET("toolsUsedList")
    Call<ToolListActiveResponse> toolsUsedList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                               @Query(AppConstants.RequestDataKey.api_token) String api_tokan);


    @GET("toolsAvailableList")
    Call<ToolListInactiveResponse> toolsAvailableList(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                                      @Query(AppConstants.RequestDataKey.api_token) String api_tokan);

    @GET("checkInOutJob")
    Call<SimpleResponse> checkInOutJob(@Query(AppConstants.RequestDataKey.user_id) String user_id,
                                       @Query(AppConstants.RequestDataKey.api_token) String api_tokan,
                                       @Query(AppConstants.RequestDataKey.job_id) String job_id,
                                       @Query(AppConstants.RequestDataKey.in_out_flag) String in_out_flag,
                                       @Query(AppConstants.RequestDataKey.date_time) String quantidate_timety);

    @POST("contact-us")
    Call<JsonElement> doContactUs(
            @Query(AppConstants.RequestDataKey.name) String name,
            @Query(AppConstants.RequestDataKey.email) String email,
            @Query(AppConstants.RequestDataKey.phone) String phone,
            @Query(AppConstants.RequestDataKey.message) String message);

    @Multipart
    @POST("special-inquiry")
    Call<JsonElement> doSpecialImages(@Part MultipartBody.Part[] files,@Part(AppConstants.RequestDataKey.note) RequestBody note,@Part(AppConstants.RequestDataKey.name) RequestBody name,@Part(AppConstants.RequestDataKey.email) RequestBody email);
}
