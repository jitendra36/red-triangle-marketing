package com.redtrianglemarketing.paginate;

import android.support.v7.widget.RecyclerView;
import com.redtrianglemarketing.recycler.RecyclerPaginate;

public abstract class Paginate {

    public interface Callbacks {
        void onLoadMore();

        boolean isLoading();
        boolean hasLoadedAllItems();
    }
    abstract public void setHasMoreDataToLoad(boolean hasMoreDataToLoad);
    abstract public void unbind();
    public static RecyclerPaginate.Builder with(RecyclerView recyclerView, Callbacks callback) {
        return new RecyclerPaginate.Builder(recyclerView, callback);
    }
}